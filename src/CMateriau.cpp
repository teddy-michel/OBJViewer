/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


/*******************************
 *  Inclusions
 ******************************/

#include "CMateriau.hpp"


/**
 * Constructeur avec le nom du matériau.
 *
 * \param name Nom du matériau.
 */

CMateriau::CMateriau ( const QString& name ) :
m_name         ( name ),
m_ambient      ( CColor ( 102 , 102 , 102 ) ),
m_diffuse      ( CColor ( 204 , 204 , 204 ) ),
m_specular     ( CColor ( 76 , 76 , 76 ) ),
m_transparency ( 1.0f ),
m_shininess    ( 60.0f ),
m_illumination ( false )
{ }


/**
 * Donne le nom du matériau.
 *
 * \return Nom du matériau.
 */

QString CMateriau::getName ( void ) const
{
    return m_name;
}


CColor CMateriau::getAmbient ( void ) const
{
    return m_ambient;
}


CColor CMateriau::getDiffuse ( void ) const
{
    return m_diffuse;
}


CColor CMateriau::getSpecular ( void ) const
{
    return m_specular;
}


float CMateriau::getTransparency ( void ) const
{
    return m_transparency;
}


float CMateriau::getShininess ( void ) const
{
    return m_shininess;
}


bool CMateriau::getIllumination ( void ) const
{
    return m_illumination;
}


/**
 * Modifie le nom du matériau.
 *
 * \param name Nouveau nom.
 */

void CMateriau::setName ( const QString& name )
{
    m_name = name;
    return;
}


void CMateriau::setAmbient ( const CColor& color )
{
    m_ambient = color;
    return;
}


void CMateriau::setDiffuse ( const CColor& color )
{
    m_diffuse = color;
    return;
}


void CMateriau::setSpecular ( const CColor& color )
{
    m_specular = color;
    return;
}


void CMateriau::setTransparency ( float transparency )
{
    if ( transparency < 0.0f ) transparency = 0.0f;
    else if ( transparency > 1.0f ) transparency = 1.0f;

    m_transparency = transparency;
    return;
}


void CMateriau::setShininess ( float shininess )
{
    if ( shininess < 0.0f ) shininess = 0.0f;

    m_shininess = shininess;
    return;
}


void CMateriau::setIllumination ( bool illum )
{
    m_illumination = illum;
    return;
}
