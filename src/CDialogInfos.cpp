/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


/*******************************
 *  Inclusions
 ******************************/

#include <sstream>

#include "CDialogInfos.hpp"
#include "CMesh.hpp"


CDialogInfos::CDialogInfos ( QWidget * parent ) :
QDialog ( parent ),
m_mesh  ( NULL ),
m_ui    ( NULL )
{
    m_ui = new Ui::CDialogInfos();
    m_ui->setupUi ( this );
}


CMesh * CDialogInfos::getMesh ( void ) const
{
    return m_mesh;
}


void CDialogInfos::setMesh ( CMesh * mesh )
{
    m_mesh = mesh;

    if ( m_mesh )
    {
        m_ui->m_lbl1->setText ( m_mesh->getFilename() );
        m_ui->m_lbl2->setNum ( static_cast<int>(m_mesh->getNbrGroups()) );
        m_ui->m_lbl3->setNum ( static_cast<int>(m_mesh->getNbrFaces()) );
        m_ui->m_lbl4->setNum ( static_cast<int>(m_mesh->getNbrSommets()) );

        CVector3F size = m_mesh->getSize();
        m_ui->m_lbl5->setText ( QString::number ( size.X ) + QString ( " x " ) + QString::number ( size.Y ) + QString ( " x " ) + QString::number ( size.Z ) );
    }

    return;
}
