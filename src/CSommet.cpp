/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


/*******************************
 *  Inclusions
 ******************************/

#include "CSommet.hpp"


/**
 * Constructeur par défaut.
 */

CSommet::CSommet ( void ) :
m_point ( Origin3F ),
m_normale ( Origin3F )
{ }


/**
 * Constructeur à partir d'un point et d'un vecteur normal.
 *
 * \param point Coordonnées du sommet.
 * \param normale Vecteur normale au sommet.
 */

CSommet::CSommet ( const CVector3F& point , const CVector3F& normale ) :
m_point ( point ),
m_normale ( normale )
{
    m_normale.Normalize();
}


/**
 * Donne les coordonnées du sommet.
 *
 * \return Coordonnées du sommet.
 */

CVector3F CSommet::getPoint ( void ) const
{
    return m_point;
}


/**
 * Donne le vecteur normal au sommet.
 *
 * \return Vecteur normale au sommet.
 */

CVector3F CSommet::getNormale ( void ) const
{
    return m_normale;
}


/**
 * Modifie les coordonnées du sommet.
 *
 * \param point Nouvelles coordonnées du sommet.
 */

void CSommet::setPoint ( const CVector3F& point )
{
    m_point = point;
    return;
}


/**
 * Modifie la normale du sommet.
 *
 * \param normale Nouvelle normale.
 */

void CSommet::setNormale ( const CVector3F& normale )
{
    m_normale = normale;
    m_normale.Normalize();
    return;
}
