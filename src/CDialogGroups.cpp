/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


/*******************************
 *  Inclusions
 ******************************/

#include "CDialogGroups.hpp"
#include "CMesh.hpp"
#include "CMateriau.hpp"


CDialogGroups::CDialogGroups ( QWidget * parent ) :
QDialog ( parent ),
m_mesh  ( NULL ),
m_ui    ( NULL ),
m_model ( NULL )
{
    m_ui = new Ui::CDialogGroups();
    m_ui->setupUi ( this );

    m_model = new QStandardItemModel ( this );
    m_ui->vue->setModel ( m_model );
}


CMesh * CDialogGroups::getMesh ( void ) const
{
    return m_mesh;
}


void CDialogGroups::setMesh ( CMesh * mesh )
{
    m_mesh = mesh;
    m_model->clear();

    QStringList headers;
    headers << trUtf8 ( "Nom" ) << trUtf8 ( "Faces" ) << trUtf8 ( "Matériau" );
    m_model->setHorizontalHeaderLabels ( headers );

    if ( m_mesh )
    {
        unsigned int nbr_groups = m_mesh->getNbrGroups();
        m_model->setRowCount ( nbr_groups );
        QStandardItem * item = NULL;

        // Pour chaque groupe
        for ( unsigned int i = 0 ; i < nbr_groups ; ++i )
        {
            CMeshGroup * group = m_mesh->getGroup ( i );

            item = new QStandardItem ( group->getName() );
            item->setEditable ( false );
            m_model->setItem ( i , 0 , item );

            QString str;
            str.setNum ( group->getNbrFaces() );

            item = new QStandardItem ( str );
            item->setEditable ( false );
            m_model->setItem ( i , 1 , item );

            if ( group->getMateriau() )
            {
                item = new QStandardItem ( group->getMateriau()->getName() );
            }
            else
            {
                item = new QStandardItem ( "" );
            }

            item->setEditable ( false );
            m_model->setItem ( i , 2 , item );
        }

        m_model->sort ( 0 );
    }

    return;
}
