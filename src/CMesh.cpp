/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


/*******************************
 *  Inclusions
 ******************************/

#include <sstream>
#include <fstream>
#include <algorithm>
#include <QtGui>

#include "CMesh.hpp"
#include "CMateriau.hpp"


#include <fstream>


/**
 * Constructeur par défaut.
 */

CMesh::CMesh ( void ) :
m_filename ( "" ),
m_header   ( "" )
{ }


/**
 * Supprime les groupes et les matériaux du maillage.
 */

CMesh::~CMesh()
{
    for ( std::list<CMeshGroup *>::iterator it = m_groups.begin() ; it != m_groups.end() ; ++it )
    {
        delete *it;
    }

    for ( std::list<CMateriau *>::iterator it = m_materiaux.begin() ; it != m_materiaux.end() ; ++it )
    {
        delete *it;
    }
}


/**
 * Donne le nom du fichier contenant le maillage.
 *
 * \return Nom du fichier.
 * \sa CMesh::getFilename
 */

QString CMesh::getName ( void ) const
{
    int pos = m_filename.lastIndexOf ( QRegExp("[\\/]" ) );

    if ( pos >= 0 )
    {
        return m_filename.mid ( pos + 1 );
    }
    else
    {
        return m_filename;
    }
}


/**
 * Donne l'adresse du fichier contenant le maillage.
 *
 * \return Adresse du fichier.
 * \sa CMesh::getName
 */

QString CMesh::getFilename ( void ) const
{
    return m_filename;
}


/**
 * Donne l'en-tête du fichier contenant le maillage.
 *
 * \return En-tête du fichier.
 * \sa CMesh::setHeader
 */

QString CMesh::getHeader ( void ) const
{
    return m_header;
}


/**
 * Donne le sommet à partir de son indice.
 *
 * \param indice Indice du sommet demandé.
 * \return Sommet.
 */

CSommet CMesh::getSommet ( unsigned int indice ) const
{
    return ( indice < m_sommets.size() ? m_sommets[indice] : CSommet ( Origin3F , Origin3F ) );
}


/**
 * Donne le groupe à partir de son indice.
 *
 * \param indice Indice du groupe demandé.
 * \return Groupe.
 */

CMeshGroup * CMesh::getGroup ( unsigned int indice ) const
{
    if ( indice < m_groups.size() )
    {
        std::list<CMeshGroup *>::const_iterator it = m_groups.begin();
        std::advance ( it , indice );
        return *it;
    }

    return NULL;
}


/**
 * Donne le matériau à partir de son indice.
 *
 * \param indice Indice du matériau demandé.
 * \return Matériau.
 */

CMateriau * CMesh::getMateriau ( unsigned int indice ) const
{
    if ( indice < m_materiaux.size() )
    {
        std::list<CMateriau *>::const_iterator it = m_materiaux.begin();
        std::advance ( it , indice );
        return *it;
    }

    return NULL;
}


/**
 * Modifie l'en-tête du fichier contenant le maillage.
 *
 * \param header Nouvelle en-tête.
 * \sa CMesh::getHeader
 */

void CMesh::setHeader ( const QString& header )
{
    m_header = header;
    return;
}


/**
 * Charge le maillage depuis un fichier.
 *
 * \param filename Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 */

bool CMesh::LoadFromFile ( const QString& filename )
{
    QFile file ( filename );

    if ( !file.open ( QIODevice::ReadOnly ) )
    {
        return false;
    }

    m_filename = filename;


    // Suppression des anciennes données
    for ( std::list<CMeshGroup *>::iterator it = m_groups.begin() ; it != m_groups.end() ; ++it )
    {
        delete *it;
    }

    for ( std::list<CMateriau *>::iterator it = m_materiaux.begin() ; it != m_materiaux.end() ; ++it )
    {
        delete *it;
    }

    m_groups.clear();
    m_materiaux.clear();
    m_sommets.clear();


    CMeshGroup * group = NULL;
    CMateriau * mtl = NULL;
    unsigned int nbr_sommets = 0;
    QTextStream stream ( &file );

    // Pour chaque ligne du fichier
    for ( QString line = stream.readLine() ; !line.isNull() ; line = stream.readLine() )
    {
        QTextStream iss ( &line );

        QString type;
        iss >> type;

        // Vertex
        if ( type == "v" )
        {
            CVector3F vertex;
            iss >> vertex;

            m_sommets.push_back ( CSommet ( vertex , Origin3F ) );
            ++nbr_sommets;
        }
        // Vecteurs normaux
        else if ( type == "vn" )
        {
            CVector3F vertex;
            iss >> vertex;

            //...
        }
        // Coordonnées de texture
        else if ( type == "vt" )
        {
            float u = 0.0f, v = 0.0f, w = 0.0f;

            iss >> u;
            iss >> v;

            // Texture 2D
            if ( iss.status() == QTextStream::Ok )
            {
                //...
            }

            iss >> w;

            // Texture 3D
            if ( iss.status() == QTextStream::Ok )
            {
                //...
            }
        }
        // Face
        else if ( type == "f" )
        {
            char slash;
            int index1 = 1, index2 = 1, index3 = 1, index4 = 1;
            unsigned int s1 = 0, s2 = 0, s3 = 0;

            iss >> index1;
            iss >> slash;

            // Si les trois indices sont présents, on passe au sommet suivant
            if ( slash == '/' )
            {
                do
                {
                    iss >> slash;
                }
                while ( slash != ' ' && !iss.atEnd() );
            }

            if ( index1 < 0 ) s1 = nbr_sommets + index1;
            else if ( index1 > 0 ) s1 = index1 - 1;
            else continue;

            iss >> index2;
            iss >> slash;

            // Si les trois indices sont présents, on passe au sommet suivant
            if ( slash == '/' )
            {
                do
                {
                    iss >> slash;
                }
                while ( slash != ' ' && !iss.atEnd() );
            }

            if ( index2 < 0 ) s2 = nbr_sommets + index2;
            else if ( index1 > 0 ) s2 = index2 - 1;
            else continue;

            iss >> index3;
            iss >> slash;

            // Si les trois indices sont présents, on passe au sommet suivant
            if ( slash == '/' )
            {
                do
                {
                    iss >> slash;
                }
                while ( slash != ' ' && !iss.atEnd() );
            }

            if ( index3 < 0 ) s3 = nbr_sommets + index3;
            else if ( index3 > 0 ) s3 = index3 - 1;
            else continue;

            // Création du groupe
            if ( !group )
            {
                group = new CMeshGroup ( this );
                m_groups.push_back ( group );
            }

            // On vérifie que les indices sont corrects pour ajouter la face
            if ( s1 < nbr_sommets && s2 < nbr_sommets && s3 < nbr_sommets &&
                 s1 != s2 && s1 != s3 && s2 != s3 )
            {
                group->AddFace ( s1 , s2 , s3 );
            }

            // Découpage de la face en triangles
            for ( iss >> index4 ; iss.status() == QTextStream::Ok ; iss >> index4 )
            {
                iss >> slash;

                // Si les trois indices sont présents, on passe au sommet suivant
                if ( slash == '/' )
                {
                    do
                    {
                        iss >> slash;
                    }
                    while ( slash != ' ' && !iss.atEnd() );
                }

                s2 = s3;

                if ( index4 < 0 ) s3 = nbr_sommets + index4;
                else if ( index4 > 0 ) s3 = index4 - 1;
                else continue;

                // On vérifie que les indices sont corrects pour ajouter la face
                if ( s1 < nbr_sommets && s2 < nbr_sommets && s3 < nbr_sommets &&
                     s1 != s2 && s1 != s3 && s2 != s3 )
                {
                    group->AddFace ( s1 , s2 , s3 );
                }
            }
        }
        // Groupe
        else if ( type == "g" )
        {
            group = NULL;
            QString grp_name;
            iss >> grp_name;

            // On cherche si le nom est déjà utilisé
            for ( std::list<CMeshGroup *>::iterator it = m_groups.begin() ; it != m_groups.end() ; ++it )
            {
                if ( (*it)->getName() == grp_name )
                {
                    group = *it;
                    break;
                }
            }

            // Création du groupe
            if ( !group )
            {
                group = new CMeshGroup ( this , grp_name );
                if ( mtl ) group->setMateriau ( mtl );
                m_groups.push_back ( group );
            }
        }
        // Fichier de matériaux
        else if ( type == "mtllib" )
        {
            QString mtl_file;
            iss >> mtl_file;

            int pos = m_filename.lastIndexOf ( QRegExp("[\\/]" ) );

            if ( pos >= 0 )
            {
                mtl_file = m_filename.mid ( 0 , pos ) + "/" + mtl_file;
            }

            // Chargement du fichier MTL
            ReadMTL ( mtl_file );
        }
        // Matériau
        else if ( type == "usemtl" )
        {
            QString mtl_name;
            iss >> mtl_name;

            // On cherche si le matériau existe
            for ( std::list<CMateriau *>::iterator it = m_materiaux.begin() ; it != m_materiaux.end() ; ++it )
            {
                if ( (*it)->getName() == mtl_name )
                {
                    mtl = *it;
                    break;
                }
            }

            if ( mtl && group )
            {
                group->setMateriau ( mtl );
            }
        }
    }

    // Suppression des groupes vides
    for ( std::list<CMeshGroup *>::iterator it = m_groups.begin() ; it != m_groups.end() ; )
    {
        if ( (*it)->getNbrFaces() == 0 )
        {
            delete *it;
            it = m_groups.erase ( it );
        }
		else
		{
			++it;
		}
    }


    // On centre le maillage sur l'origine
    CVector3F min = Origin3F, max = Origin3F;
    bool first = false;

    for ( std::vector<CSommet>::iterator it = m_sommets.begin() ; it != m_sommets.end() ; ++it )
    {
        CVector3F pt = it->getPoint();

        if ( first )
        {
            if ( pt.X < min.X ) min.X = pt.X;
            if ( pt.Y < min.Y ) min.Y = pt.Y;
            if ( pt.Z < min.Z ) min.Z = pt.Z;

            if ( pt.X > max.X ) max.X = pt.X;
            if ( pt.Y > max.Y ) max.Y = pt.Y;
            if ( pt.Z > max.Z ) max.Z = pt.Z;
        }
        else
        {
            min = max = pt;
            first = true;
        }
    }

    CVector3F tmp = max - min;
    float scale = qMax ( tmp.X , qMax ( tmp.Y , tmp.Z ) );
    CVector3F translation = ( max + min ) / 2.0f;

    // Pour chaque sommet
    for ( int i = 0 ; i < m_sommets.size() ; ++i )
    {
        m_sommets[i].setPoint ( ( m_sommets[i].getPoint() - translation ) / scale );
    }


    // Calcul des normales
    for ( unsigned int i = 0 ; i < nbr_sommets ; ++i )
    {
        ComputeNormaleSommet ( i );
    }

    return true;
}


/**
 * Enregistre le maillage dans un fichier.
 *
 * \todo Supprimer les doublons des listes des points et des vecteurs normaux.
 * \param filename Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 */

bool CMesh::SaveToFile ( const QString& filename ) const
{
    QFile file ( filename );

    if ( !file.open ( QIODevice::WriteOnly ) )
    {
        return false;
    }

    QTextStream stream ( &file );

    // Écriture de l'en-tête du fichier
    stream << m_header << endl;

    // Matériaux
    if ( m_materiaux.size() > 0 )
    {
        QString mtl_file = filename.mid ( 0 , filename.lastIndexOf('.') );
        mtl_file += ".mtl";

        WriteMTL ( mtl_file );

        int pos = mtl_file.lastIndexOf ( QRegExp("[\\/]" ) );

        if ( pos >= 0 )
        {
            mtl_file = mtl_file.mid ( pos + 1 );
        }

        stream << "mtllib " << mtl_file << endl << endl;
    }

    // Écritures de la liste des points
    for ( std::vector<CSommet>::const_iterator it = m_sommets.begin() ; it != m_sommets.end() ; ++it )
    {
        stream << "v " << it->getPoint() << endl;
    }

    stream << "# " << m_sommets.size() << " vertices" << endl << endl;

    // Écritures de la liste des vecteurs normaux
    for ( std::vector<CSommet>::const_iterator it = m_sommets.begin() ; it != m_sommets.end() ; ++it )
    {
        stream << "vn " << it->getNormale() << endl;
    }

    stream << "# " << m_sommets.size() << " vertex normals" << endl << endl;

    // Écritures de la liste des faces de chaque groupe
    for ( std::list<CMeshGroup *>::const_iterator it = m_groups.begin() ; it != m_groups.end() ; ++it )
    {
        unsigned int nbr_faces = (*it)->getNbrFaces();

        if ( nbr_faces == 0 ) continue;

        stream << "g " << (*it)->getName() << endl;

        if ( (*it)->getMateriau() )
        {
            stream << "usemtl " << (*it)->getMateriau()->getName() << endl;
        }

        stream << endl;

        for ( unsigned int i = 0 ; i < nbr_faces ; ++i )
        {
            CFace f = (*it)->getFace ( i );
            stream << "f " << ( f.s1 + 1 ) << " " << ( f.s2 + 1 ) << " " << ( f.s3 + 1 ) << endl;
        }

        stream << "# " << nbr_faces << " faces" << endl << endl;
    }

    return true;
}


/**
 * Donne le nombre de groupes du maillage.
 *
 * \return Nombre de groupes.
 */

unsigned int CMesh::getNbrGroups ( void ) const
{
    return m_groups.size();
}


/**
 * Donne le nombre de sommets du maillage.
 *
 * \return Nombre de sommets.
 */

unsigned int CMesh::getNbrSommets ( void ) const
{
    return m_sommets.size();
}


/**
 * Donne le nombre de faces du maillage.
 *
 * \return Nombre de faces.
 */

unsigned int CMesh::getNbrFaces ( void ) const
{
    unsigned int n = 0;

    for ( std::list<CMeshGroup *>::const_iterator it = m_groups.begin() ; it != m_groups.end() ; ++it )
    {
        n += (*it)->getNbrFaces();
    }

    return n;
}


/**
 * Donne le nombre de matériaux utilisés par le maillage.
 *
 * \return Nombre de matériaux.
 */

unsigned int CMesh::getNbrMateriaux ( void ) const
{
    return m_materiaux.size();
}


/**
 * Ajoute un sommet au maillage.
 *
 * \param sommet Sommet à ajouter.
 * \return Indice du sommet ajouté.
 */

unsigned int CMesh::AddSommet ( const CSommet& sommet )
{
    CVector3F p = sommet.getPoint();
    CVector3F n = sommet.getNormale();

    unsigned int indice = 0;

    // Pour chaque sommet
    for ( std::vector<CSommet>::iterator it = m_sommets.begin() ; it != m_sommets.end() ; ++it )
    {
        // Le sommet existe déjà
        if ( p == it->getPoint() && n == it->getNormale() )
        {
            return indice;
        }

        ++indice;
    }

    // Le sommet n'existe pas
    m_sommets.push_back ( sommet );
    return ( m_sommets.size() - 1 );
}


/**
 * Subdivise le maillage de manière plane.
 */

void CMesh::SubdivisionPlane ( void )
{
    // Pour chaque groupe
    for ( std::list<CMeshGroup *>::iterator it = m_groups.begin() ; it != m_groups.end() ; ++it )
    {
        (*it)->SubdivisionPlane();
    }

    return;
}


/**
 * Subdivise le maillage en utilisant les normales.
 */

void CMesh::SubdivisionNormale ( void )
{
    // Pour chaque groupe
    for ( std::list<CMeshGroup *>::iterator it = m_groups.begin() ; it != m_groups.end() ; ++it )
    {
        (*it)->SubdivisionNormale();
    }

    return;
}


/**
 * Filtre le maillage.
 *
 * \param coefs Coefficients du filtre.
 */

void CMesh::Filtre ( const std::vector<float>& coefs )
{
    unsigned int niveau = coefs.size();

    std::vector<CSommet> tmp_sommets;
    tmp_sommets.reserve ( m_sommets.size() );
    unsigned int i = 0;

    // Pour chaque sommet
    for ( std::vector<CSommet>::const_iterator it = m_sommets.begin() ; it != m_sommets.end() ; ++it )
    {
        CVector3F point = Origin3F;
        CVector3F normale = Origin3F;

        std::vector<std::vector<unsigned int> > voisins = SommetsVoisins ( i++ , niveau );

        // Pour chaque coefficient du filtre
        for ( unsigned int j = 0 ; j < niveau ; ++j )
        {
            if ( coefs[j] == 0.0f ) continue;

            unsigned int nbr_voisins = voisins[j].size();

            if ( nbr_voisins == 0 ) continue;

            CVector3F tmp_point = Origin3F;
            CVector3F tmp_normale = Origin3F;

            for ( std::vector<unsigned int>::iterator it2 = voisins[j].begin() ; it2 != voisins[j].end() ; ++it2 )
            {
                CSommet s = getSommet ( *it2 );
                tmp_point += s.getPoint();
                tmp_normale += s.getNormale();
            }

            point += tmp_point * coefs[j] / nbr_voisins;
            normale += tmp_normale * coefs[j] / nbr_voisins;
        }

        tmp_sommets.push_back ( CSommet ( point , normale ) );
    }

    // Suppression des anciens sommets
    m_sommets = tmp_sommets;

    return;
}


/**
 * Redimensionne le maillage en utilisant le même facteur sur chaque axe.
 *
 * \param scale Facteur de redimensionnement (strictement positif).
 */

void CMesh::Scale ( float scale )
{
    Scale ( scale , scale , scale );
    return;
}


/**
 * Redimensionne le maillage.
 *
 * \param x Facteur de redimensionnement selon X (strictement positif).
 * \param y Facteur de redimensionnement selon Y (strictement positif).
 * \param z Facteur de redimensionnement selon Z (strictement positif).
 */

void CMesh::Scale ( float x , float y , float z )
{
    // Valeurs incorrectes
    if ( x < 0.0f || y < 0.0f || z < 0.0f )
    {
        return;
    }

    // On modifie les coordonnées de chaque sommet
    for ( std::vector<CSommet>::iterator it = m_sommets.begin() ; it != m_sommets.end() ; ++it )
    {
        CVector3F pt = it->getPoint();
        it->setPoint ( CVector3F ( pt.X * x , pt.Y * y , pt.Z * z ) );
    }

    return;
}


/**
 * Effectue une rotation du maillage.
 *
 * \todo Implémentation.
 *
 * \param x Angle selon l'axe X en degrés.
 * \param y Angle selon l'axe Y en degrés.
 * \param z Angle selon l'axe Z en degrés.
 */

void CMesh::Rotate ( float x , float y , float z )
{
    //...

    // Pour chaque sommet
    for ( std::vector<CSommet>::iterator it = m_sommets.begin() ; it != m_sommets.end() ; ++it )
    {
        //...
    }

    return;
}


/**
 * Donne le point minimal de la boite englobante.
 *
 * \return Point minimal.
 */

CVector3F CMesh::getMin ( void ) const
{
    CVector3F min = Origin3F;
    bool first = true;

    // Pour chaque sommet
    for ( std::vector<CSommet>::const_iterator it = m_sommets.begin() ; it != m_sommets.end() ; ++it )
    {
        CVector3F pt = it->getPoint();

        if ( first )
        {
            min = pt;
            first = false;
        }
        else
        {
            if ( pt.X < min.X ) min.X = pt.X;
            if ( pt.Y < min.Y ) min.Y = pt.Y;
            if ( pt.Z < min.Z ) min.Z = pt.Z;
        }
    }

    return min;
}


/**
 * Donne le point maximal de la boite englobante.
 *
 * \return Point maximal.
 */

CVector3F CMesh::getMax ( void ) const
{
    CVector3F max = Origin3F;
    bool first = true;

    // Pour chaque sommet
    for ( std::vector<CSommet>::const_iterator it = m_sommets.begin() ; it != m_sommets.end() ; ++it )
    {
        CVector3F pt = it->getPoint();

        if ( first )
        {
            max = pt;
            first = false;
        }
        else
        {
            if ( pt.X > max.X ) max.X = pt.X;
            if ( pt.Y > max.Y ) max.Y = pt.Y;
            if ( pt.Z > max.Z ) max.Z = pt.Z;
        }
    }

    return max;
}


/**
 * Donne les dimensions du maillage.
 *
 * \return Dimensions du maillage.
 */

CVector3F CMesh::getSize ( void ) const
{
    CVector3F min = Origin3F, max = Origin3F;
    bool first = true;

    // Pour chaque sommet
    for ( std::vector<CSommet>::const_iterator it = m_sommets.begin() ; it != m_sommets.end() ; ++it )
    {
        CVector3F pt = it->getPoint();

        if ( first )
        {
            min = max = pt;
            first = false;
        }
        else
        {
            if ( pt.X < min.X ) min.X = pt.X;
            if ( pt.Y < min.Y ) min.Y = pt.Y;
            if ( pt.Z < min.Z ) min.Z = pt.Z;

            if ( pt.X > max.X ) max.X = pt.X;
            if ( pt.Y > max.Y ) max.Y = pt.Y;
            if ( pt.Z > max.Z ) max.Z = pt.Z;
        }
    }

    return ( max - min );
}


/**
 * Donne la liste des sommets voisins d'un autre sommet.
 *
 * \param sommet Sommet dont on veut connaitre les voisins.
 * \param niveau Niveau maximal de recherche (0 = le sommet lui-même, 1 = ses voisins immédiats, etc.).
 * \return Liste des sommets voisins pour chaque niveau.
 */

std::vector< std::vector<unsigned int> > CMesh::SommetsVoisins ( unsigned int sommet , unsigned int niveau ) const
{
    std::vector<std::vector<unsigned int> > voisins;

    // Le sommet n'existe pas
    if ( sommet >= m_sommets.size() )
    {
        return voisins;
    }

    // Niveau 0 : le sommet lui-même
    if ( niveau == 0 )
    {
        std::vector<unsigned int> v0;
        v0.push_back ( sommet );
        voisins.push_back ( v0 );
        return voisins;
    }

    if ( niveau == 1 )
    {
        std::vector<unsigned int> v0;
        v0.push_back ( sommet );
        std::vector<unsigned int> v1;

        // Pour chaque groupe
        for ( std::list<CMeshGroup *>::const_iterator it = m_groups.begin() ; it != m_groups.end() ; ++it )
        {
            unsigned int nbr_faces = (*it)->getNbrFaces();

            // Pour chaque face
            for ( unsigned int i = 0 ; i < nbr_faces ; ++i )
            {
                CFace f = (*it)->getFace ( i );

                if ( f.s1 == sommet )
                {
                    if ( f.s2 != sommet && std::find ( v1.begin() , v1.end() , f.s2 ) == v1.end() ) v1.push_back ( f.s2 );
                    if ( f.s3 != sommet && std::find ( v1.begin() , v1.end() , f.s3 ) == v1.end() ) v1.push_back ( f.s3 );
                }
                else if ( f.s2 == sommet )
                {
                    if ( f.s1 != sommet && std::find ( v1.begin() , v1.end() , f.s1 ) == v1.end() ) v1.push_back ( f.s1 );
                    if ( f.s3 != sommet && std::find ( v1.begin() , v1.end() , f.s3 ) == v1.end() ) v1.push_back ( f.s3 );
                }
                else if ( f.s3 == sommet )
                {
                    if ( f.s1 != sommet && std::find ( v1.begin() , v1.end() , f.s1 ) == v1.end() ) v1.push_back ( f.s1 );
                    if ( f.s2 != sommet && std::find ( v1.begin() , v1.end() , f.s2 ) == v1.end() ) v1.push_back ( f.s2 );
                }
            }
        }

        voisins.push_back ( v0 );
        voisins.push_back ( v1 );

        return voisins;
    }

    voisins = SommetsVoisins ( sommet , niveau - 1 );
    std::vector<unsigned int> vn;

    // Pour chaque voisin de niveau n - 1
    for ( std::vector<unsigned int>::iterator it1 = voisins[niveau - 1].begin() ; it1 != voisins[niveau - 1].end() ; ++it1 )
    {
        // On cherche les voisins de niveau 1
        std::vector<std::vector<unsigned int> > voisins_tmp = SommetsVoisins ( *it1 , 1 );

        // Pour chaque voisins de niveau 1
        for ( std::vector<unsigned int>::iterator it2 = voisins_tmp[1].begin() ; it2 != voisins_tmp[1].end() ; ++it2 )
        {
            // Si on ne l'a pas déjà ajouté, on l'ajoute aux voisins de niveau n
            if ( *it2 != sommet && std::find ( vn.begin() , vn.end() , *it2 ) == vn.end() )
            {
                vn.push_back ( *it2 );
            }
        }
    }

    // Suppression des doublons
    for ( unsigned int i = 0 ; i < niveau ; ++i )
    {
        // Pour chaque voisin de niveau n
        for ( std::vector<unsigned int>::iterator it = voisins[niveau].begin() ; it != voisins[niveau].end() ; ++it )
        {
            // Si le sommet est présent parmi les voisins de niveau inférieur, on l'enlève des voisins de niveau n
            if ( std::find ( voisins[i].begin() , voisins[i].end() , *it ) != voisins[i].end() )
            {
                it = vn.erase ( it );
            }
        }
    }

    voisins.push_back ( vn );
    return voisins;
}


std::vector<CFace> CMesh::FacesVoisines ( const CFace& face ) const
{
    std::vector<CFace> faces;

    // Pour chaque groupe
    for ( std::list<CMeshGroup *>::const_iterator it = m_groups.begin() ; it != m_groups.end() ; ++it )
    {
        unsigned int nbr_faces = (*it)->getNbrFaces();

        // Pour chaque face
        for ( unsigned int i = 0 ; i < nbr_faces ; ++i )
        {
            CFace f = (*it)->getFace ( i );

            if ( !( face.s1 == f.s1 && face.s2 == f.s2 && face.s3 == f.s3 ) && (
                 ( ( f.s1 == face.s1 || f.s2 == face.s1 || f.s3 == face.s1 ) &&
                   ( f.s1 == face.s2 || f.s2 == face.s2 || f.s3 == face.s2 ) ) ||
                 ( ( f.s1 == face.s2 || f.s2 == face.s2 || f.s3 == face.s2 ) &&
                   ( f.s1 == face.s3 || f.s2 == face.s3 || f.s3 == face.s3 ) ) ||
                 ( ( f.s1 == face.s1 || f.s2 == face.s1 || f.s3 == face.s1 ) &&
                   ( f.s1 == face.s3 || f.s2 == face.s3 || f.s3 == face.s3 ) ) ) )
            {
                faces.push_back ( f );
            }
        }
    }

    return faces;
}


std::vector<CFace> CMesh::FacesIncidentes ( unsigned int indice ) const
{
    std::vector<CFace> faces;

    // Pour chaque groupe
    for ( std::list<CMeshGroup *>::const_iterator it = m_groups.begin() ; it != m_groups.end() ; ++it )
    {
        unsigned int nbr_faces = (*it)->getNbrFaces();

        // Pour chaque face
        for ( unsigned int i = 0 ; i < nbr_faces ; ++i )
        {
            CFace f = (*it)->getFace ( i );

            if ( f.s1 == indice || f.s2 == indice || f.s3 == indice )
            {
                faces.push_back ( f );
            }
        }
    }

    return faces;
}


void CMesh::ComputeNormaleSommet ( unsigned int indice )
{
    if ( indice >= m_sommets.size() || m_groups.size() < 1 ) return;

    CMeshGroup * group = m_groups.front();

    std::vector<CFace> faces = FacesIncidentes ( indice );
    const unsigned int nbr_faces = faces.size();

    std::vector<CVector3F> normales_faces;

    // Pour chaque face incidente au sommet
    for ( unsigned int i = 0 ; i < nbr_faces ; ++i )
    {
        bool existe = false;
        CVector3F normale_face = group->NormaleFace ( faces[i] );

        // On cherche la normale de la face dans le tableau des normales
        for ( std::vector<CVector3F>::const_iterator it = normales_faces.begin() ; it != normales_faces.end() ; ++it )
        {
            if ( *it == normale_face )
            {
                existe = true;
            }
        }

        if ( !existe )
        {
            normales_faces.push_back ( normale_face );
        }
    }

    CVector3F normale = Origin3F;

    // Pour chaque normale des faces incidentes au sommet
    for ( std::vector<CVector3F>::const_iterator it = normales_faces.begin() ; it != normales_faces.end() ; ++it )
    {
        normale += *it;
    }

    m_sommets[indice].setNormale ( normale );
}


/**
 * Supprime les sommets identiques dans le maillage.
 * Deux sommets sont identiques s'ils ont les mêmes coordonnées et la même normale.
 */

void CMesh::SupprimeSommetsIdentiques ( void )
{
    // Pour chaque sommet
    for ( std::vector<CSommet>::iterator it1 = m_sommets.begin() ; it1 != m_sommets.end() ; ++it1 )
    {
        CVector3F pt1 = it1->getPoint();
        CVector3F n1 = it1->getNormale();

        unsigned int d1 = std::distance ( m_sommets.begin() , it1 );

        // Pour chaque sommet suivant
        for ( std::vector<CSommet>::iterator it2 = it1 + 1 ; it2 != m_sommets.end() ; )
        {
            unsigned int d2 = std::distance ( m_sommets.begin() , it2 );

            // Les deux sommets sont identiques
            if ( pt1 == it2->getPoint() && n1 == it2->getNormale() )
            {
                // Pour chaque groupe du maillage
                for ( std::list<CMeshGroup *>::iterator itg = m_groups.begin() ; itg != m_groups.end() ; ++itg )
                {

                    (*itg)->RemplaceIndiceSommet ( d2 , d1 );
                }

                // Suppression du deuxième sommet
                it2 = m_sommets.erase ( it2 );

                unsigned int nbr_sommets = m_sommets.size();

                // On décale tous les indices suivants
                for ( unsigned int i = d2 + 1 ; i <= nbr_sommets ; ++i )
                {
                    for ( std::list<CMeshGroup *>::iterator itg = m_groups.begin() ; itg != m_groups.end() ; ++itg )
                    {
                        (*itg)->RemplaceIndiceSommet ( i , i - 1 );
                    }
                }
            }
            else
            {
                ++it2;
            }
        }
    }

    return;
}


/**
 * Lit un fichier MTL et crée les matériaux.
 *
 * \param filename Adresse du fichier MTL.
 * \return Booléen indiquant le succès de l'opération.
 */

bool CMesh::ReadMTL ( const QString& filename )
{
    QFile file ( filename );

    if ( !file.open ( QIODevice::ReadOnly ) )
    {
        return false;
    }

    CMateriau * mtl = NULL;
    QTextStream stream ( &file );

    // Pour chaque ligne du fichier
    for ( QString line = stream.readLine() ; !line.isNull() ; line = stream.readLine() )
    {
        QTextStream iss ( &line );

        QString type;
        iss >> type;

        // Matériau
        if ( type == "newmtl" )
        {
            QString mtl_name;
            iss >> mtl_name;

            // Création du matériau
            mtl = new CMateriau ( mtl_name );
            m_materiaux.push_back ( mtl );
        }
        else if ( mtl )
        {
            // Couleur ambiante
            if ( type == "Ka" )
            {
                float c1, c2, c3;
                iss >> c1 >> c2 >> c3;

                mtl->setAmbient ( CColor ( static_cast<unsigned char>(c1 * 255) ,
                                           static_cast<unsigned char>(c2 * 255) ,
                                           static_cast<unsigned char>(c3 * 255) ) );
            }
            // Couleur diffuse
            else if ( type == "Kd" )
            {
                float c1, c2, c3;
                iss >> c1 >> c2 >> c3;

                mtl->setDiffuse ( CColor ( static_cast<unsigned char>(c1 * 255) ,
                                           static_cast<unsigned char>(c2 * 255) ,
                                           static_cast<unsigned char>(c3 * 255) ) );
            }
            // Couleur spéculaire
            else if ( type == "Ks" )
            {
                float c1, c2, c3;
                iss >> c1 >> c2 >> c3;

                mtl->setSpecular ( CColor ( static_cast<unsigned char>(c1 * 255) ,
                                            static_cast<unsigned char>(c2 * 255) ,
                                            static_cast<unsigned char>(c3 * 255) ) );
            }
            // Illumination
            else if ( type == "illum" )
            {
                int i;
                iss >> i;

                if ( i == 1 ) mtl->setIllumination ( false );
                else if ( i == 2 ) mtl->setIllumination ( true );
            }
            // Transparence
            else if ( type == "d" || type == "Tr" )
            {
                float tr = 1.0f;
                iss >> tr;

                mtl->setTransparency ( tr );
            }
            // Brillance
            else if ( type == "Ns" )
            {
                float ns = 0.0f;
                iss >> ns;

                mtl->setShininess ( ns );
            }
        }
    }

    file.close();

    return true;
}


/**
 * Écrit le fichier MTL contenant les matériaux utilisés par le maillage.
 *
 * \param filename Adresse du fichier MTL.
 * \return Booléen indiquant le succès de l'opération.
 */

bool CMesh::WriteMTL ( const QString& filename ) const
{
    QFile file ( filename );

    if ( !file.open ( QIODevice::WriteOnly ) )
    {
        return false;
    }

    QTextStream stream ( &file );

    // Écritures de la liste des matériaux
    for ( std::list<CMateriau *>::const_iterator it = m_materiaux.begin() ; it != m_materiaux.end() ; ++it )
    {
        CColor color;
        float a;

        stream << endl << "newmtl " << (*it)->getName() << endl;

        color = (*it)->getAmbient();

        if ( color.getRed() != 102 || color.getGreen() != 102 || color.getBlue() != 102 )
        {
            stream << "Ka ";
            stream << ( static_cast<float>(color.getRed())   / 255.0f ) << ' ';
            stream << ( static_cast<float>(color.getGreen()) / 255.0f ) << ' ';
            stream << ( static_cast<float>(color.getBlue())  / 255.0f ) << endl;
        }

        color = (*it)->getDiffuse();

        if ( color.getRed() != 204 || color.getGreen() != 204 || color.getBlue() != 204 )
        {
            stream << "Kd ";
            stream << ( static_cast<float>(color.getRed())   / 255.0f ) << ' ';
            stream << ( static_cast<float>(color.getGreen()) / 255.0f ) << ' ';
            stream << ( static_cast<float>(color.getBlue())  / 255.0f ) << endl;
        }

        color = (*it)->getSpecular();

        if ( color.getRed() != 76 || color.getGreen() != 76 || color.getBlue() != 76 )
        {
            stream << "Ks ";
            stream << ( static_cast<float>(color.getRed())   / 255.0f ) << ' ';
            stream << ( static_cast<float>(color.getGreen()) / 255.0f ) << ' ';
            stream << ( static_cast<float>(color.getBlue())  / 255.0f ) << endl;
        }

        a = (*it)->getTransparency();
        if ( a >= 0.0f && a < 1.0f ) stream << "Tr " << a << endl;

        a = (*it)->getShininess();
        if ( a > 0.0f ) stream << "Ns " << a << endl;

        stream << "illum " << ( (*it)->getIllumination() ? 2 : 1 ) << endl;
    }

    file.close();

    return true;
}
