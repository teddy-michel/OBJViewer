/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


/*******************************
 *  Inclusions
 ******************************/

#include "CMeshGroup.hpp"
#include "CMesh.hpp"
#include "CMateriau.hpp"
#include "CSommet.hpp"


/**
 * Constructeur.
 *
 * \param mesh Mesh auquel appartient le groupe.
 * \param name Nom du groupe.
 */

CMeshGroup::CMeshGroup ( CMesh * mesh , const QString& name ) :
m_name     ( name ),
m_materiau ( NULL ),
m_mesh     ( mesh )
{ }


/**
 * Donne le nom du groupe.
 *
 * \return Nom du groupe.
 */

QString CMeshGroup::getName ( void ) const
{
    return m_name;
}


/**
 * Donne le matériau du groupe.
 *
 * \return Pointeur sur le matériau.
 */

CMateriau * CMeshGroup::getMateriau ( void ) const
{
    return m_materiau;
}


/**
 * Donne le maillage contenant le groupe.
 *
 * \return Pointeur sur le maillage contenant le groupe.
 */

CMesh * CMeshGroup::getMesh ( void ) const
{
    return m_mesh;
}


/**
 * Donne une face du groupe.
 *
 * \param indice Indice de la face voulue.
 * \return Face.
 */

CFace CMeshGroup::getFace ( unsigned int indice ) const
{
    return ( indice < m_faces.size() ? m_faces[indice] : CFace() );
}


/**
 * Modifie le nom du groupe.
 *
 * \param name Nouveau nom.
 */

void CMeshGroup::setName ( const QString& name )
{
    m_name = name;
    return;
}


/**
 * Modifie le matériau du groupe.
 *
 * \param materiau Pointeur sur le nouveau matériau.
 */

void CMeshGroup::setMateriau ( CMateriau * materiau )
{
    m_materiau = materiau;
    return;
}


/**
 * Modifie le mesh du groupe.
 *
 * \param mesh Maillage auquel appartient le groupe.
 */

void CMeshGroup::setMesh ( CMesh * mesh )
{
    m_mesh = mesh;
    return;
}


/**
 * Donne le nombre de faces du groupe.
 *
 * \return Nombre de faces.
 */

unsigned int CMeshGroup::getNbrFaces ( void ) const
{
    return m_faces.size();
}


/**
 * Donne le vecteur normal à une face.
 *
 * \param indice Indice de la face voulue.
 * \return Vecteur normal de la face.
 */

CVector3F CMeshGroup::NormaleFace ( unsigned int indice ) const
{
    if ( m_mesh == NULL || indice >= m_faces.size() ) return Origin3F;
    return NormaleFace ( m_faces[indice] );
}


/**
 * Donne le vecteur normal à une face.
 *
 * \param face Face dont on veut connaitre la normale.
 * \return Vecteur normal de la face.
 */

CVector3F CMeshGroup::NormaleFace ( const CFace& face ) const
{
    if ( m_mesh == NULL ) return Origin3F;

    CVector3F s1 = m_mesh->getSommet ( face.s1 ).getPoint();
    CVector3F s2 = m_mesh->getSommet ( face.s2 ).getPoint();
    CVector3F s3 = m_mesh->getSommet ( face.s3 ).getPoint();

    CVector3F normale = VectorCross ( s2 - s1 , s3 - s1 );
    normale.Normalize();

    return normale;
}


/**
 * Donne le barycentre d'une face.
 *
 * \param indice Indice de la face voulue.
 * \return Barycentre de la face.
 */

CVector3F CMeshGroup::BarycentreFace ( unsigned int indice ) const
{
    if ( m_mesh == NULL || indice >= m_faces.size() ) return Origin3F;
    return BarycentreFace ( m_faces[indice] );
}


/**
 * Donne le barycentre d'une face.
 *
 * \param face Face dont on veut connaitre la normale.
 * \return Barycentre de la face.
 */

CVector3F CMeshGroup::BarycentreFace ( const CFace& face ) const
{
    if ( m_mesh == NULL ) return Origin3F;

    CVector3F s1 = m_mesh->getSommet ( face.s1 ).getPoint();
    CVector3F s2 = m_mesh->getSommet ( face.s2 ).getPoint();
    CVector3F s3 = m_mesh->getSommet ( face.s3 ).getPoint();

    return ( ( s1 + s2 + s3 ) / 3.0f );
}


/**
 * Ajoute une face au groupe.
 *
 * \param s1 Indice du premier sommet de la face.
 * \param s2 Indice du deuxième sommet de la face.
 * \param s3 Indice du troisième sommet de la face.
 */

void CMeshGroup::AddFace ( unsigned int s1 , unsigned int s2 , unsigned int s3 )
{
    // Indices identiques
    if ( s1 == s2 || s1 == s3 || s2 == s3 )
    {
        return;
    }

    m_faces.push_back ( CFace ( s1 , s2 , s3 ) );
    return;
}


/**
 * Subdivise le groupe de manière plane.
 */

void CMeshGroup::SubdivisionPlane ( void )
{
    if ( m_mesh == NULL ) return;

    std::vector<CFace> newfaces;
    newfaces.reserve ( 4 * m_faces.size() );

    // Pour chaque face du groupe
    for ( std::vector<CFace>::const_iterator it = m_faces.begin() ; it != m_faces.end() ; ++it )
    {
        CSommet s1 = m_mesh->getSommet ( it->s1 );
        CSommet s2 = m_mesh->getSommet ( it->s2 );
        CSommet s3 = m_mesh->getSommet ( it->s3 );

        // Coordonnées des sommets
        CVector3F p1 = s1.getPoint();
        CVector3F p2 = s2.getPoint();
        CVector3F p3 = s3.getPoint();

        // Normales des sommets
        CVector3F n1 = s1.getNormale();
        CVector3F n2 = s2.getNormale();
        CVector3F n3 = s3.getNormale();

        // Milieux des arêtes
        CSommet m12 ( ( p1 + p2 ) * 0.5f , n1 + n2 );
        CSommet m13 ( ( p1 + p3 ) * 0.5f , n1 + n3 );
        CSommet m23 ( ( p2 + p3 ) * 0.5f , n2 + n3 );

        unsigned int s12 = m_mesh->AddSommet ( m12 );
        unsigned int s13 = m_mesh->AddSommet ( m13 );
        unsigned int s23 = m_mesh->AddSommet ( m23 );

        // Création des nouvelles faces
        newfaces.push_back ( CFace ( it->s1 , s12 , s13 ) );
        newfaces.push_back ( CFace ( s12 , it->s2 , s23 ) );
        newfaces.push_back ( CFace ( s13 , s23 , it->s3 ) );
        newfaces.push_back ( CFace ( s12 , s23 , s13 ) );
    }

    // Mise-à-jour des faces du groupe
    m_faces = newfaces;

    return;
}


/**
 * Subdivise le groupe en utilisant les normales.
 *
 * \todo Implantation.
 */

void CMeshGroup::SubdivisionNormale ( void )
{
    if ( m_mesh == NULL ) return;

    std::vector<CFace> newfaces;
    newfaces.reserve ( 4 * m_faces.size() );

    // Pour chaque face du groupe
    for ( std::vector<CFace>::const_iterator it = m_faces.begin() ; it != m_faces.end() ; ++it )
    {
        // Sommets du triangle
        CSommet s1 = m_mesh->getSommet ( it->s1 );
        CSommet s2 = m_mesh->getSommet ( it->s2 );
        CSommet s3 = m_mesh->getSommet ( it->s3 );

        // Coordonnées des sommets
        CVector3F p1 = s1.getPoint();
        CVector3F p2 = s2.getPoint();
        CVector3F p3 = s3.getPoint();

        // Normales des sommets
        CVector3F n1 = s1.getNormale();
        CVector3F n2 = s2.getNormale();
        CVector3F n3 = s3.getNormale();

        // Distances entre les sommets
        CVector3F tmp12 = p2 - p1;
        float d12 = tmp12.Norme();
        CVector3F tmp13 = p3 - p1;
        float d13 = tmp13.Norme();
        CVector3F tmp23 = p3 - p2;
        float d23 = tmp23.Norme();

        // Nouveaux points
        CVector3F pt1 = ( p1 + p2 ) * 0.5f + ( n1 + n2 ) * 0.05f * d12;
                                                         //( sqrt ( 2 ) * 0.25f + sqrt ( 2 ) * 0.5f * d12 );
        CVector3F pt2 = ( p1 + p3 ) * 0.5f + ( n1 + n3 ) * 0.05f * d13;
                                                         //( sqrt ( 2 ) * 0.25f + sqrt ( 2 ) * 0.5f * d13 );
        CVector3F pt3 = ( p2 + p3 ) * 0.5f + ( n2 + n3 ) * 0.05f * d23;
                                                         //( sqrt ( 2 ) * 0.25f + sqrt ( 2 ) * 0.5f * d23 );

        // Nouveaux sommets
        unsigned int s12 = m_mesh->AddSommet ( CSommet ( pt1 , n1 + n2 ) );
        unsigned int s13 = m_mesh->AddSommet ( CSommet ( pt2 , n1 + n3 ) );
        unsigned int s23 = m_mesh->AddSommet ( CSommet ( pt3 , n2 + n3 ) );

        // Création des nouvelles faces
        newfaces.push_back ( CFace ( it->s1 , s12 , s13 ) );
        newfaces.push_back ( CFace ( s12 , it->s2 , s23 ) );
        newfaces.push_back ( CFace ( s13 , s23 , it->s3 ) );
        newfaces.push_back ( CFace ( s12 , s23 , s13 ) );
    }

    // Mise-à-jour des faces du groupe
    m_faces = newfaces;

    return;
}


void CMeshGroup::RemplaceIndiceSommet ( unsigned int old_indice , unsigned int new_indice )
{
    // Pour chaque face
    for ( std::vector<CFace>::iterator it = m_faces.begin() ; it != m_faces.end() ; ++it )
    {
        if ( it->s1 == old_indice ) it->s1 = new_indice;
        if ( it->s2 == old_indice ) it->s2 = new_indice;
        if ( it->s3 == old_indice ) it->s3 = new_indice;
    }

    return;
}
