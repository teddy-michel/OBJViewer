/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


/*******************************
 *  Inclusions
 ******************************/

#include "CWindow.hpp"
#include "CMesh.hpp"
#include "CDraw.hpp"
#include "CDialogInfos.hpp"
#include "CDialogGroups.hpp"
#include "CDialogMateriaux.hpp"
#include "CDialogScale.hpp"
#include "CDialogRotate.hpp"


/**
 * Constructeur par défaut.
 */

CWindow::CWindow ( void ) :
m_mesh              ( NULL ),
m_draw              ( NULL ),
m_dialog_infos      ( NULL ),
m_dialog_groups     ( NULL ),
m_dialog_materiaux  ( NULL ),
m_dialog_scale      ( NULL ),
m_dialog_rotate     ( NULL ),
m_menu_fichier      ( NULL ),
m_menu_modele       ( NULL ),
m_menu_affichage    ( NULL ),
m_menu_aide         ( NULL ),
m_action_save       ( NULL ),
m_action_saveas     ( NULL ),
m_action_close      ( NULL ),
m_action_infos      ( NULL ),
m_action_groupes    ( NULL ),
m_action_materiaux  ( NULL ),
m_action_scale      ( NULL ),
m_action_rotate     ( NULL ),
m_action_subplane   ( NULL ),
m_action_subnormale ( NULL ),
m_action_filtrage   ( NULL )
{
    setMinimumSize ( 400 , 300 );


    m_dialog_infos = new CDialogInfos ( this );
    m_dialog_groups = new CDialogGroups ( this );
    m_dialog_materiaux = new CDialogMateriaux ( this );
    m_dialog_scale = new CDialogScale ( this );
    m_dialog_rotate = new CDialogRotate ( this );


    m_draw = new CDraw();
    setCentralWidget ( m_draw );

    QAction * action_ouvrir = new QAction ( trUtf8 ( "Ouvrir") , this );
    connect ( action_ouvrir , SIGNAL(triggered()) , this , SLOT(OpenFile()) );
    action_ouvrir->setShortcut ( QKeySequence ( QKeySequence::Open ) );
    action_ouvrir->setStatusTip ( trUtf8 ( "Ouvre un fichier contenant un maillage" ) );

    m_action_save = new QAction ( trUtf8 ( "Enregistrer" ) , this );
    connect ( m_action_save , SIGNAL(triggered()) , this , SLOT(SaveFile()) );
    m_action_save->setShortcut ( QKeySequence ( QKeySequence::Save ) );
    m_action_save->setStatusTip ( trUtf8 ( "Enregistre le maillage affiché" ) );
    m_action_save->setEnabled ( false );

    m_action_saveas = new QAction ( trUtf8 ( "Enregistrer sous..." ) , this );
    connect ( m_action_saveas , SIGNAL(triggered()) , this , SLOT(SaveFileAs()) );
    m_action_saveas->setShortcut ( QKeySequence ( QKeySequence::SaveAs ) );
    m_action_saveas->setStatusTip ( trUtf8 ( "Enregistre le maillage affiché dans un nouveau fichier" ) );
    m_action_saveas->setEnabled ( false );

    m_action_close = new QAction ( trUtf8 ( "Fermer" ) , this );
    connect ( m_action_close , SIGNAL(triggered()) , this , SLOT(Close()) );
    m_action_close->setShortcut ( QKeySequence ( QKeySequence::Close ) );
    m_action_close->setStatusTip ( trUtf8 ( "Ferme le maillage affiché" ) );
    m_action_close->setEnabled ( false );

    QAction * action_quitter = new QAction ( trUtf8 ( "Quitter" ) , this );
    connect ( action_quitter , SIGNAL(triggered()) , qApp , SLOT(quit()) );
    action_quitter->setShortcut ( QKeySequence ( QKeySequence::Quit ) );
    action_quitter->setStatusTip ( trUtf8 ( "Quitte le programme" ) );

    m_action_infos = new QAction ( trUtf8 ( "Informations sur le maillage" ) , this );
    connect ( m_action_infos , SIGNAL(triggered()) , this , SLOT(DialogInfos()) );
    m_action_infos->setEnabled ( false );

    m_action_groupes = new QAction ( trUtf8 ( "Groupes" ) , this );
    connect ( m_action_groupes , SIGNAL(triggered()) , this , SLOT(DialogGroups()) );
    m_action_groupes->setEnabled ( false );

    m_action_materiaux = new QAction ( trUtf8 ( "Matériaux" ) , this );
    connect ( m_action_materiaux , SIGNAL(triggered()) , this , SLOT(DialogMateriaux()) );
    m_action_materiaux->setEnabled ( false );

    m_action_scale = new QAction ( trUtf8 ( "Redimensionner le maillage" ) , this );
    connect ( m_action_scale , SIGNAL(triggered()) , this , SLOT(DialogScale()) );
    m_action_scale->setEnabled ( false );

    m_action_rotate = new QAction ( trUtf8 ( "Tourner le maillage" ) , this );
    connect ( m_action_rotate , SIGNAL(triggered()) , this , SLOT(DialogRotate()) );
    m_action_rotate->setEnabled ( false );

    m_action_subplane = new QAction ( trUtf8 ( "Subdivision plane" ) , this );
    connect ( m_action_subplane , SIGNAL(triggered()) , this , SLOT(SubdivisionPlane()) );
    m_action_subplane->setEnabled ( false );

    m_action_subnormale = new QAction ( trUtf8 ( "Subdivision normale" ) , this );
    connect ( m_action_subnormale , SIGNAL(triggered()) , this , SLOT(SubdivisionNormale()) );
    m_action_subnormale->setEnabled ( false );

    m_action_filtrage = new QAction ( trUtf8 ( "Filtrage gaussien" ) , this );
    connect ( m_action_filtrage , SIGNAL(triggered()) , this , SLOT(Filtrage()) );
    m_action_filtrage->setEnabled ( false );

    QAction * action_aff_repere = new QAction ( trUtf8 ( "Afficher le repère" ) , this );
    connect ( action_aff_repere , SIGNAL(triggered()) , this , SLOT(AffRepere()) );
    action_aff_repere->setCheckable ( true );

    QAction * action_aff_boundingbox = new QAction ( trUtf8 ( "Afficher la boite englobante" ) , this );
    connect ( action_aff_boundingbox , SIGNAL(triggered()) , this , SLOT(AffBoundingBox()) );
    action_aff_boundingbox->setCheckable ( true );

    QAction * action_aff_spherebox = new QAction ( trUtf8 ( "Afficher la sphère englobante" ) , this );
    connect ( action_aff_spherebox , SIGNAL(triggered()) , this , SLOT(AffSphereBox()) );
    action_aff_spherebox->setCheckable ( true );

    QAction * action_aff_normales = new QAction ( trUtf8 ( "Afficher les normales" ) , this );
    connect ( action_aff_normales , SIGNAL(triggered()) , this , SLOT(AffNormales()) );
    action_aff_normales->setCheckable ( true );

    QAction * action_aff_culling = new QAction ( trUtf8 ( "Activer le culling" ) , this );
    connect ( action_aff_culling , SIGNAL(triggered()) , this , SLOT(AffCulling()) );
    action_aff_culling->setCheckable ( true );
    action_aff_culling->setChecked ( true );

    QAction * action_aff_smooth = new QAction ( trUtf8 ( "Utiliser les normales des faces" ) , this );
    connect ( action_aff_smooth , SIGNAL(triggered()) , this , SLOT(AffSmooth()) );
    action_aff_smooth->setCheckable ( true );

    QAction * action_aff_wireframe = new QAction ( trUtf8 ( "Mode fil-de-fer" ) , this );
    connect ( action_aff_wireframe , SIGNAL(triggered()) , this , SLOT(AffWireframe()) );
    action_aff_wireframe->setCheckable ( true );

    QAction * action_aboutqt = new QAction ( trUtf8 ( "À propos de Qt" ) , this );
    connect ( action_aboutqt , SIGNAL(triggered()) , qApp , SLOT(aboutQt()) );


    m_menu_fichier = menuBar()->addMenu ( trUtf8 ( "Fichier" ) );

    m_menu_fichier->addAction ( action_ouvrir );
    m_menu_fichier->addAction ( m_action_save );
    m_menu_fichier->addAction ( m_action_saveas );
    m_menu_fichier->addAction ( m_action_close );
    m_menu_fichier->addSeparator();
    m_menu_fichier->addAction ( action_quitter );

    m_menu_modele = menuBar()->addMenu ( trUtf8 ( "Modèle" ) );
    m_menu_modele->setEnabled ( false );

    m_menu_modele->addAction ( m_action_infos );
    m_menu_modele->addAction ( m_action_groupes );
    m_menu_modele->addAction ( m_action_materiaux );
    m_menu_modele->addSeparator();
    m_menu_modele->addAction ( m_action_scale );
    m_menu_modele->addAction ( m_action_rotate );
    m_menu_modele->addAction ( m_action_subplane );
    m_menu_modele->addAction ( m_action_subnormale );
    m_menu_modele->addAction ( m_action_filtrage );

    m_menu_affichage = menuBar()->addMenu ( trUtf8 ( "Affichage" ) );

    m_menu_affichage->addAction ( action_aff_repere );
    m_menu_affichage->addAction ( action_aff_boundingbox );
    //m_menu_affichage->addAction ( action_aff_spherebox );
    m_menu_affichage->addAction ( action_aff_normales );
    m_menu_affichage->addSeparator();
    m_menu_affichage->addAction ( action_aff_culling );
    m_menu_affichage->addAction ( action_aff_smooth );
    m_menu_affichage->addAction ( action_aff_wireframe );

    m_menu_aide = menuBar()->addMenu ( trUtf8 ( "Aide" ) );

    m_menu_aide->addAction ( action_aboutqt );


    resize ( 800 , 600 );
    setWindowTitle ( "OBJViewer" );
    statusBar()->showMessage ( trUtf8 ( "Aucun maillage chargé" ) , 5000 );
}


/**
 * Destructeur.
 */

CWindow::~CWindow()
{
    delete m_mesh;
}


/**
 * Ouvre un fichier.
 *
 * \todo Effectuer le chargement dans un autre thread.
 */

void CWindow::OpenFile ( void )
{
    QString fichier = QFileDialog::getOpenFileName ( this , trUtf8 ( "Ouvrir un fichier" ) , QString() , trUtf8 ( "Fichier OBJ (*.obj)" ) );

    if ( fichier.isEmpty() )
    {
        return;
    }

    CMesh * mesh = new CMesh();
    statusBar()->showMessage ( trUtf8 ( "Chargement du fichier %1..." ).arg ( fichier ) , 1000 );

    // On mesure le temps de chargement
    QTime time;
    time.start();

    //CLoadingThread thread ( fichier );
    //connect ( &thread , SIGNAL(fileLoadingEnd()) , this , SLOT(onFileLoadingEnd()) );
    //connect ( &thread , SIGNAL(fileLoadingFail()) , this , SLOT(onFileLoadingFail()) );
    //thread.start();

    // Chargement du nouveau maillage
    if ( mesh->LoadFromFile ( fichier ) )
    {
        // Suppression de l'ancien maillage
        delete m_mesh;
        m_mesh = mesh;
        
        int duration = time.elapsed();

        setWindowTitle ( m_mesh->getName() + " - OBJViewer" );
        statusBar()->showMessage ( trUtf8 ( "Chargement du fichier %1 terminé (%2 secondes)" ).arg ( fichier ).arg( static_cast<float>(duration) / 1000 ) , 5000 );

        // Modification des menus
        m_action_save->setEnabled ( true );
        m_action_saveas->setEnabled ( true );
        m_action_close->setEnabled ( true );

        m_menu_modele->setEnabled ( true );
        m_action_infos->setEnabled ( true );
        m_action_groupes->setEnabled ( true );
        m_action_materiaux->setEnabled ( true );
        m_action_scale->setEnabled ( true );
        m_action_rotate->setEnabled ( true );
        m_action_subplane->setEnabled ( true );
        m_action_subnormale->setEnabled ( true );
        m_action_filtrage->setEnabled ( true );

        // Modification des boites de dialogue
        m_dialog_infos->setMesh ( m_mesh );
        m_dialog_groups->setMesh ( m_mesh );
        m_dialog_materiaux->setMesh ( m_mesh );
        m_dialog_scale->setMesh ( m_mesh );
        m_draw->setMesh ( m_mesh );
    }
    else
    {
        statusBar()->showMessage ( trUtf8 ( "Erreur lors du chargement du fichier %1" ).arg ( fichier ) , 5000 );
    }

    return;
}


void CWindow::SaveFile ( void )
{
    SaveFile ( m_mesh->getFilename() );
    return;
}


void CWindow::SaveFileAs ( void )
{
    QString fichier = QFileDialog::getSaveFileName ( this , trUtf8 ( "Enregistrer un fichier" ) , QString() , trUtf8 ( "Fichier OBJ (*.obj)" ) );

    if ( fichier.isEmpty() )
    {
        return;
    }

    SaveFile ( fichier );
    return;
}


/**
 * Ferme la fenêtre.
 */

void CWindow::Close ( void )
{
    delete m_mesh;
    m_mesh = NULL;

    m_dialog_infos->setMesh ( NULL );
    m_dialog_groups->setMesh ( NULL );
    m_dialog_materiaux->setMesh ( NULL );
    m_dialog_scale->setMesh ( NULL );
    m_draw->setMesh ( NULL );

    m_action_save->setEnabled ( false );
    m_action_saveas->setEnabled ( false );
    m_action_close->setEnabled ( false );

    m_menu_modele->setEnabled ( false );
    m_action_infos->setEnabled ( false );
    m_action_groupes->setEnabled ( false );
    m_action_materiaux->setEnabled ( false );
    m_action_scale->setEnabled ( false );
    m_action_rotate->setEnabled ( false );
    m_action_subplane->setEnabled ( false );
    m_action_subnormale->setEnabled ( false );
    m_action_filtrage->setEnabled ( false );

    setWindowTitle ( "OBJViewer" );

    return;
}


/**
 * Affiche la boite de dialogue sur les informations du maillage.
 */

void CWindow::DialogInfos ( void )
{
    if ( m_dialog_infos )
    {
        m_dialog_infos->show();
        m_dialog_infos->setFocus();
    }

    return;
}


/**
 * Affiche la boite de dialogue sur les groupes du maillage.
 */

void CWindow::DialogGroups ( void )
{
    if ( m_dialog_groups )
    {
        m_dialog_groups->show();
        m_dialog_groups->setFocus();
    }

    return;
}


/**
 * Affiche la boite de dialogue pour le redimensionnement.
 */

void CWindow::DialogScale ( void )
{
    if ( m_dialog_scale )
    {
        m_dialog_scale->show();
        m_dialog_scale->setFocus();
    }

    return;
}


/**
 * Affiche la boite de dialogue pour la rotation.
 */

void CWindow::DialogRotate ( void )
{
    if ( m_dialog_rotate )
    {
        m_dialog_rotate->show();
        m_dialog_rotate->setFocus();
    }

    return;
}


void CWindow::DialogMateriaux ( void )
{
    if ( m_dialog_materiaux )
    {
        m_dialog_materiaux->show();
        m_dialog_materiaux->setFocus();
    }

    return;
}


void CWindow::SubdivisionPlane ( void )
{
    if ( m_mesh )
    {
        m_mesh->SubdivisionPlane();
        if ( m_dialog_infos ) m_dialog_infos->setMesh ( m_mesh );
        if ( m_draw ) m_draw->updateGL();
    }

    return;
}


void CWindow::SubdivisionNormale ( void )
{
    if ( m_mesh )
    {
        m_mesh->SubdivisionNormale();
        if ( m_dialog_infos ) m_dialog_infos->setMesh ( m_mesh );
        if ( m_draw ) m_draw->updateGL();
    }

    return;
}


void CWindow::Filtrage ( void )
{
    if ( m_mesh )
    {
        std::vector<float> coefs;
        coefs.push_back ( 0.398940f );
        coefs.push_back ( 0.353927f );
        coefs.push_back ( 0.247133f );
    
        m_mesh->Filtre ( coefs );
        if ( m_dialog_infos ) m_dialog_infos->setMesh ( m_mesh );
        if ( m_draw ) m_draw->updateGL();
    }

    return;
}


void CWindow::AffRepere ( void )
{
    if ( m_draw ) m_draw->ToggleRepere();
    return;
}


void CWindow::AffBoundingBox ( void )
{
    if ( m_draw ) m_draw->ToggleBoundingBox();
    return;
}


void CWindow::AffSphereBox ( void )
{
    if ( m_draw ) m_draw->ToggleSphereBox();
    return;
}


void CWindow::AffNormales ( void )
{
    if ( m_draw ) m_draw->ToggleNormales();
    return;
}


void CWindow::AffCulling ( void )
{
    if ( m_draw ) m_draw->ToggleCulling();
    return;
}


void CWindow::AffSmooth ( void )
{
    if ( m_draw ) m_draw->ToggleSmooth();
    return;
}


void CWindow::AffWireframe ( void )
{
    if ( m_draw ) m_draw->ToggleWireframe();
    return;
}


void CWindow::SaveFile ( const QString& filename ) const
{
    if ( m_mesh->SaveToFile ( filename ) )
    {
        statusBar()->showMessage ( trUtf8 ( "Modèle enregistré dans %1" ).arg ( m_mesh->getFilename() ) , 5000 );
    }
    else
    {
        statusBar()->showMessage ( trUtf8 ( "Erreur lors de l'enregistrement du modèle dans %1" ).arg ( m_mesh->getFilename() ) , 5000 );
    }
}
