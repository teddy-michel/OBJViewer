/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


/*******************************
 *  Inclusions
 ******************************/

#include "CDialogRotate.hpp"
#include "CMesh.hpp"


/**
 * Constructeur de la boite de dialogue pour tourner un maillage.
 *
 * \param parent Widget parent.
 */

CDialogRotate::CDialogRotate ( QWidget * parent ) :
QDialog ( parent ),
m_mesh  ( NULL ),
m_ui    ( NULL )
{
    m_ui = new Ui::CDialogRotate();
    m_ui->setupUi ( this );

    connect ( m_ui->buttonBox , SIGNAL ( clicked ( QAbstractButton * ) ) , this , SLOT ( onButtonClicked ( QAbstractButton * ) ) );
}


/**
 * Donne le maillage actuellement chargé.
 *
 * \return Maillage chargé.
 */

CMesh * CDialogRotate::getMesh ( void ) const
{
    return m_mesh;
}


/**
 * Modifie le maillage chargé.
 *
 * \param mesh Maillage chargé.
 */

void CDialogRotate::setMesh ( CMesh * mesh )
{
    m_mesh = mesh;
    return;
}


/**
 * Applique les modifications et ferme la boite de dialogue.
 */

void CDialogRotate::onButtonClicked ( QAbstractButton * button )
{
    if ( m_ui->buttonBox->buttonRole ( button ) != QDialogButtonBox::ApplyRole )
    {
        return;
    }

    if ( m_mesh )
    {
        int vx = m_ui->spin_x->value();
        int vy = m_ui->spin_y->value();
        int vz = m_ui->spin_z->value();

        if ( vx != 0 || vy != 0 || vz != 0 )
        {
            m_mesh->Rotate ( static_cast<float>(vx) , static_cast<float>(vy) , static_cast<float>(vz) );
        }
    }

    m_ui->spin_x->setValue ( 0 );
    m_ui->spin_y->setValue ( 0 );
    m_ui->spin_z->setValue ( 0 );

    hide();
    return;
}
