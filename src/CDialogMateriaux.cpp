/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


/*******************************
 *  Inclusions
 ******************************/

#include <sstream>

#include "CDialogMateriaux.hpp"
#include "CMesh.hpp"
#include "CMateriau.hpp"


/**
 * Constructeur de la boite de dialogue des matériaux.
 *
 * \param parent Widget parent.
 */

CDialogMateriaux::CDialogMateriaux ( QWidget * parent ) :
QDialog ( parent ),
m_mesh  ( NULL ),
m_ui    ( NULL ),
m_model ( NULL )
{
    m_ui = new Ui::CDialogMateriaux();
    m_ui->setupUi ( this );

    m_model = new QStandardItemModel ( this );
    m_ui->vue->setModel ( m_model );
}


CMesh * CDialogMateriaux::getMesh ( void ) const
{
    return m_mesh;
}


void CDialogMateriaux::setMesh ( CMesh * mesh )
{
    m_mesh = mesh;
    m_model->clear();

    QStringList headers;

    headers << trUtf8 ( "Nom" );
    headers << trUtf8 ( "Couleur ambiante" );
    headers << trUtf8 ( "Couleur diffuse" );
    headers << trUtf8 ( "Couleur spéculaire" );
    headers << trUtf8 ( "Transparence" );
    headers << trUtf8 ( "Brillance" );
    headers << trUtf8 ( "Illumination" );

    m_model->setHorizontalHeaderLabels ( headers );

    if ( m_mesh )
    {
        unsigned int nbr_mtl = m_mesh->getNbrMateriaux();
        m_model->setRowCount ( nbr_mtl );
        QStandardItem * item = NULL;

        // Pour chaque matériau
        for ( unsigned int i = 0 ; i < nbr_mtl ; ++i )
        {
            CMateriau * mtl = m_mesh->getMateriau ( i );
            QString str;

            // Nom du matériau
            item = new QStandardItem ( mtl->getName() );
            item->setEditable ( false );
            m_model->setItem ( i , 0 , item );

            // Couleur ambiante
            {
                item = new QStandardItem ( mtl->getAmbient().toString() );
                item->setEditable ( false );
                m_model->setItem ( i , 1 , item );
            }

            // Couleur diffuse
            {
                item = new QStandardItem ( mtl->getDiffuse().toString() );
                item->setEditable ( false );
                m_model->setItem ( i , 2 , item );
            }

            // Couleur spéculaire
            {
                item = new QStandardItem ( mtl->getSpecular().toString() );
                item->setEditable ( false );
                m_model->setItem ( i , 3 , item );
            }

            // Transparence
            item = new QStandardItem ( QString::number ( mtl->getTransparency() ) );
            item->setEditable ( false );
            m_model->setItem ( i , 4 , item );

            // Brillance
            item = new QStandardItem ( QString::number ( mtl->getShininess() ) );
            item->setEditable ( false );
            m_model->setItem ( i , 5 , item );

            // Illumination
            if ( mtl->getIllumination() )
            {
                item = new QStandardItem ( "2" );
            }
            else
            {
                item = new QStandardItem ( "1" );
            }

            item->setEditable ( false );
            m_model->setItem ( i , 6 , item );
        }

        m_model->sort ( 0 );
    }

    return;
}
