/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


/*******************************
 *  Inclusion
 ******************************/

#include "CColor.hpp"


/*******************************
 *  Couleurs prédéfinies
 ******************************/

const CColor CColor::Blanc ( 255 , 255 , 255 );
const CColor CColor::Gris ( 128 , 128 , 128 );
const CColor CColor::Noir ( 0 , 0 , 0 );
const CColor CColor::Rouge ( 255 , 0 , 0 );
const CColor CColor::Vert ( 0 , 255 , 0 );
const CColor CColor::Bleu ( 0 , 0 , 255 );
const CColor CColor::Jaune ( 255 , 255 , 0 );
const CColor CColor::Cyan ( 0 , 255 , 255 );
const CColor CColor::Magenta ( 255 , 0 , 255 );
const CColor CColor::VertF ( 0 , 128 , 0 );
const CColor CColor::Orange ( 255 , 128 , 0 );


/**
 * Constructeur par défaut.
 *
 * \param color Valeur de la couleur.
 ******************************/

CColor::CColor ( unsigned int color ) :
m_color (color)
{ }


/**
 * Constructeur depuis 4 entiers.
 *
 * \param r Niveau de rouge.
 * \param g Niveau de vert.
 * \param b Niveau de bleu.
 * \param a Canal alpha.
 ******************************/

CColor::CColor ( unsigned char r , unsigned char g , unsigned char b , unsigned char a )
{
    set ( r , g , b , a );
}


/**
 * Accesseur pour couleur.
 *
 * \return Valeur de la couleur.
 ******************************/

unsigned int CColor::getColor ( void ) const
{
    return m_color;
}


/**
 * Comparaison ==.
 *
 * \param color Couleur à comparer.
 * \return Booléen.
 ******************************/

bool CColor::operator == ( const CColor& color ) const
{
    return ( m_color == color.getColor() );
}


/**
 * Comparaison !=.
 *
 * \param color Couleur à comparer.
 * \return Booléen.
 ******************************/

bool CColor::operator != ( const CColor& color ) const
{
    return ( m_color != color.getColor() );
}


/**
 * Opérateur +=.
 *
 * \param color Couleur à ajouter.
 * \return Nouvelle couleur.
 ******************************/

const CColor& CColor::operator += ( const CColor& color )
{
    int R = getRed()   + color.getRed();
    int G = getGreen() + color.getGreen();
    int B = getBlue()  + color.getBlue();
    int A = getAlpha() + color.getAlpha();

    setInt ( R , G , B , A );

    return *this;
}


/**
 * Opérateur -=.
 *
 * \param color Couleur à soustraire.
 * \return Nouvelle couleur.
 ******************************/

const CColor& CColor::operator -= ( const CColor& color )
{
    int R = getRed()   - color.getRed();
    int G = getGreen() - color.getGreen();
    int B = getBlue()  - color.getBlue();
    int A = getAlpha() - color.getAlpha();

    setInt ( R , G , B , A );

    return *this;
}


/**
 * Opérateur binaire +.
 *
 * \param color Couleur à modifier.
 * \return Nouvelle couleur.
 ******************************/

CColor CColor::operator + ( const CColor& color ) const
{
    int R = getRed()   + color.getRed();
    int G = getGreen() + color.getGreen();
    int B = getBlue()  + color.getBlue();
    int A = getAlpha() + color.getAlpha();

    CColor newc;
    newc.setInt ( R , G , B , A );

    return newc;
}


/**
 * Opérateur binaire -.
 *
 * \param color Couleur à modifier.
 * \return Nouvelle couleur.
 ******************************/

CColor CColor::operator - ( const CColor& color ) const
{
    int R = getRed()   - color.getRed();
    int G = getGreen() - color.getGreen();
    int B = getBlue()  - color.getBlue();
    int A = getAlpha() - color.getAlpha();

    CColor newc;
    newc.setInt ( R , G , B , A );

    return newc;
}


/**
 * Multiplication par un scalaire.
 *
 * \param k Scalaire.
 * \return Nouvelle couleur.
 ******************************/

CColor CColor::operator * ( float k ) const
{
    int R = static_cast<int> ( getRed() * k );
    int G = static_cast<int> ( getGreen() * k );
    int B = static_cast<int> ( getBlue() * k );
    int A = static_cast<int> ( getAlpha() * k );

    CColor newc;
    newc.setInt ( R , G , B , A );

    return newc;
}


/**
 * Opérateur *=.
 *
 * \param k Scalaire.
 * \return Nouvelle couleur.
 ******************************/

const CColor& CColor::operator *= ( float k )
{
    int R = static_cast<int> ( getRed() * k );
    int G = static_cast<int> ( getGreen() * k );
    int B = static_cast<int> ( getBlue() * k );
    int A = static_cast<int> ( getAlpha() * k );

    setInt ( R , G , B , A );

    return *this;
}


/**
 * Division par un scalaire.
 *
 * \param k scalaire.
 * \return Nouvelle couleur.
 ******************************/

CColor CColor::operator / ( float k ) const
{
    return *this * ( 1.0f / k );
}


/**
 * Opérateur /=.
 *
 * \param k Scalaire.
 * \return Nouvelle couleur.
 ******************************/

const CColor& CColor::operator /= ( float k )
{
    return *this *= ( 1.0f / k );
}


/**
 * Retourne la couleur en nuance de gris.
 *
 * \return Couleur transformée en gris.
 ******************************/

unsigned char CColor::toGris ( void ) const
{
    return static_cast<unsigned char> ( getRed() * 0.30 + getGreen() * 0.59 + getBlue() * 0.11 );
}


/**
 * Retourne la couleur dans le format ARGB.
 *
 * \return Couleur en ARGB.
 ******************************/

unsigned int CColor::toARGB ( void ) const
{
    return ( getAlpha() << 24 ) | ( getRed() << 16 ) | ( getGreen() << 8) | ( getBlue() << 0 );
}


/**
 * Retourne la couleur dans le format ABGR.
 *
 * \return Couleur en ABGR.
 ******************************/

unsigned int CColor::toABGR ( void ) const
{
    return ( getAlpha() << 24 ) | ( getBlue() << 16 ) | ( getGreen() << 8 ) | ( getRed() << 0 );
}


/**
 * Retourne la couleur dans le format RGBA.
 *
 * \return Couleur en RGBA.
 ******************************/

unsigned int CColor::toRGBA ( void ) const
{
    return ( getRed() << 24 ) | ( getGreen() << 16 ) | ( getBlue() << 8 ) | ( getAlpha() << 0 );
}


/**
 * Retourne le canal alpha.
 *
 * \return Canal alpha.
 ******************************/

unsigned char CColor::getAlpha () const
{
    return static_cast<unsigned char> ( ( m_color & 0xFF000000 ) >> 24 );
}


/**
 * Retourne le canal rouge.
 *
 * \return Canal rouge.
 ******************************/

unsigned char CColor::getRed ( void ) const
{
    return static_cast<unsigned char> ( ( m_color & 0x00FF0000 ) >> 16 );
}


/**
 * Retourne le canal vert.
 *
 * \return Canal vert.
 ******************************/

unsigned char CColor::getGreen ( void ) const
{
    return static_cast<unsigned char> ( ( m_color & 0x0000FF00 ) >> 8 );
}


/**
 * Retourne le canal bleu.
 *
 * \return Canal bleu.
 ******************************/

unsigned char CColor::getBlue ( void ) const
{
    return static_cast<unsigned char> ( ( m_color & 0x000000FF ) >> 0 );
}


/**
 * Modifie la couleur à partir de 4 réels.
 *
 * \param r Pourcentage de rouge.
 * \param g Pourcentage de vert.
 * \param b Pourcentage de bleu.
 * \param a Pourcentage alpha.
 ******************************/

void CColor::set ( float r , float g , float b , float a )
{
    int R = static_cast<int> ( r * 255.0f );
    int G = static_cast<int> ( g * 255.0f );
    int B = static_cast<int> ( b * 255.0f );
    int A = static_cast<int> ( a * 255.0f );

    setInt ( R , G , B , A );

    return;
}


/**
 * Modifie la couleur à partir de 4 composantes.
 *
 * \param r Niveau de rouge.
 * \param g Niveau de vert.
 * \param b Niveau de bleu.
 * \param a Canal alpha.
 ******************************/

void CColor::set ( unsigned char r , unsigned char g , unsigned char b , unsigned char a )
{
    m_color = ( a << 24 ) | ( r << 16 ) | ( g << 8 ) | ( b << 0 );
    return;
}


/**
 * Ajoute deux couleurs.
 *
 * \param color Couleur à ajouter.
 * \return Nouvelle couleur.
 ******************************/

CColor CColor::add ( const CColor& color ) const
{
    return *this + color;
}


/**
 * Module deux couleurs.
 *
 * \param color Couleur avec laquelle moduler.
 * \return Nouvelle couleur.
 ******************************/

CColor CColor::modulate ( const CColor& color ) const
{
    unsigned char R = static_cast<unsigned char> ( getRed()   * color.getRed()   / 255 );
    unsigned char G = static_cast<unsigned char> ( getGreen() * color.getGreen() / 255 );
    unsigned char B = static_cast<unsigned char> ( getBlue()  * color.getBlue()  / 255 );
    unsigned char A = static_cast<unsigned char> ( getAlpha() * color.getAlpha() / 255 );

    return CColor ( R , G , B , A );
}


/**
 * Convertit en 4 floats RGBA.
 *
 * \param dest Tableau de destination.
 ******************************/

void CColor::toFloat ( float dest[] ) const
{
    dest[0] = getRed() / 255.0f;
    dest[1] = getGreen() / 255.0f;
    dest[2] = getBlue() / 255.0f;
    dest[3] = getAlpha() / 255.0f;

    return;
}


QString CColor::toString ( void ) const
{
	return ( QString::number ( getRed()   ) + QString ( " " ) +
		     QString::number ( getGreen() ) + QString ( " " ) +
		     QString::number ( getBlue()  ) + QString ( " " ) +
		     QString::number ( getAlpha() ) );
}


/**
 * Modifie la couleur à partir de 4 entiers.
 *
 * \param r Niveau de rouge.
 * \param g Niveau de vert.
 * \param b Niveau de bleu.
 * \param a Canal alpha.
 ******************************/

void CColor::setInt ( int r , int g , int b , int a )
{
    unsigned char R = ( r >= 0 ) ? ( r <= 255 ? r : 255 ) : 0;
    unsigned char G = ( g >= 0 ) ? ( g <= 255 ? g : 255 ) : 0;
    unsigned char B = ( b >= 0 ) ? ( b <= 255 ? b : 255 ) : 0;
    unsigned char A = ( a >= 0 ) ? ( a <= 255 ? a : 255 ) : 0;

    set ( R , G , B , A );

    return;
}


/**
 * Surcharge de l'opérateur >> entre un flux et une couleur.
 *
 * \param flux Flux d'entrée.
 * \param color Couleur.
 * \return Référence sur le flux d'entrée.
 ******************************/

std::istream& operator >> ( std::istream& flux , CColor& color )
{
    unsigned int R, G, B, A;
    flux >> R >> G >> B >> A;

    color.set ( static_cast<unsigned char>(R) , static_cast<unsigned char>(G) , static_cast<unsigned char>(B)  , static_cast<unsigned char>(A) );

    return flux;
}


/**
 * Surcharge de l'opérateur << entre un flux et une couleur.
 *
 * \param flux Flux de sortie.
 * \param color Couleur.
 * \return Référence sur le flux de sortie.
 ******************************/

std::ostream& operator << ( std::ostream& flux , const CColor& color )
{
    return flux << static_cast<int>(color.getRed()) << " " << static_cast<int>(color.getGreen()) << " " << static_cast<int>(color.getBlue()) << " " << static_cast<int>(color.getAlpha());
}
