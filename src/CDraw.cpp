/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


/*******************************
 *  Inclusions
 ******************************/

#include <GL/glu.h>

#include "CDraw.hpp"
#include "CMateriau.hpp"
#include "CMesh.hpp"
#include "CMeshGroup.hpp"


/**
 * Constructeur par défaut.
 */

CDraw::CDraw ( QWidget * parent , QGLWidget * shareWidget ) :
QGLWidget ( parent , shareWidget ),
m_mesh        ( NULL ),
m_smooth      ( true ),
m_normales    ( false ),
m_repere      ( false ),
m_wireframe   ( false ),
m_culling     ( true ),
m_boundingbox ( false ),
m_spherebox   ( false ),
m_materiaux   ( true ),
m_last_pos    ( 0 , 0 ),
m_angle_z     ( 0.0f ),
m_angle_y     ( 0.0f )
{ }


/**
 * Donne le maillage à afficher.
 *
 * \return Pointeur sur le maillage à afficher.
 */

CMesh * CDraw::getMesh ( void ) const
{
    return m_mesh;
}


/**
 * Donne la taille minimale du widget.
 *
 * \return Taille minimale du widget.
 */

QSize CDraw::minimumSizeHint ( void ) const
{
    return QSize ( 100 , 100 );
}


/**
 * Donne la taille optimale du widget.
 *
 * \return Taille optimale du widget.
 */

QSize CDraw::sizeHint ( void ) const
{
    return QSize ( 200 , 200 );
}


/**
 * Modifie le maillage à afficher.
 *
 * \param mesh Pointeur sur le maillage à afficher.
 */

void CDraw::setMesh ( CMesh * mesh )
{
    m_mesh = mesh;
    updateGL();
    return;
}


/**
 * Inverse l'affichage du repère.
 */

void CDraw::ToggleRepere ( void )
{
    m_repere = !m_repere;
    updateGL();
    return;
}


/**
 * Inverse l'affichage de la boite englobante.
 */

void CDraw::ToggleBoundingBox ( void )
{
    m_boundingbox = !m_boundingbox;
    updateGL();
    return;
}


/**
 * Inverse l'affichage de la sphère englobante.
 */

void CDraw::ToggleSphereBox ( void )
{
    m_spherebox = !m_spherebox;
    updateGL();
    return;
}


/**
 * Inverse l'affichage des normales.
 */

void CDraw::ToggleNormales ( void )
{
    m_normales = !m_normales;
    updateGL();
    return;
}


/**
 * Inverse l'utilisation du culling.
 */

void CDraw::ToggleCulling ( void )
{
    m_culling = !m_culling;

    if ( m_culling ) glEnable ( GL_CULL_FACE );
    else glDisable ( GL_CULL_FACE );

    updateGL();
    return;
}


/**
 * Inverse l'utilisation des normales de faces et de sommets.
 */

void CDraw::ToggleSmooth ( void )
{
    m_smooth = !m_smooth;
    updateGL();
    return;
}


/**
 * Inverse l'affichage en mode fil-de-fer.
 */

void CDraw::ToggleWireframe ( void )
{
    m_wireframe = !m_wireframe;

    if ( m_wireframe ) glPolygonMode ( GL_FRONT_AND_BACK , GL_LINE );
    else glPolygonMode ( GL_FRONT_AND_BACK , GL_FILL );

    updateGL();
    return;
}


void CDraw::initializeGL ( void )
{
    glEnable ( GL_CULL_FACE );

    glShadeModel ( GL_SMOOTH );
    glHint ( GL_PERSPECTIVE_CORRECTION_HINT , GL_NICEST );

    glClearColor ( 0.0f , 0.0f , 0.0f , 0.0f );
    glClearDepth ( 1.0f );

    glEnable ( GL_DEPTH_TEST );
    glDepthFunc ( GL_LEQUAL );

    glEnable ( GL_COLOR_MATERIAL );
    glEnable ( GL_LIGHTING );
    glEnable ( GL_LIGHT0 );

    static GLfloat lightPosition[4] = { 0.5f , 5.0f , 7.0f , 1.0f };
    glLightfv ( GL_LIGHT0 , GL_POSITION , lightPosition );

    return;
}


/**
 * Redimensionne le contexte OpenGL.
 *
 * \param w Nouvelle largeur.
 * \param h Nouvelle hauteur.
 */

void CDraw::resizeGL ( int w , int h )
{
    h = ( h ? h : 1 );

    glViewport ( 0 , 0 , w , h );

    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity();
    gluPerspective ( 60.0f , static_cast<GLdouble>(w) / static_cast<GLdouble>(h) , 0.01f , 100.0f );

    glMatrixMode ( GL_MODELVIEW );
    glLoadIdentity();

    return;
}


/**
 * Dessiner les objets avec OpenGL.
 *
 * \todo Affichage de la sphère englobante
 */

void CDraw::paintGL ( void )
{
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glLoadIdentity();

    gluLookAt ( -2.0f , 0.0f , 0.0f ,
                 0.0f , 0.0f , 0.0f ,
                 0.0f , 1.0f , 0.0f );

    glRotatef ( m_angle_y , 0.0f , 0.0f , 1.0f );
    glRotatef ( m_angle_z , 0.0f , 1.0f , 0.0f );


    if ( m_mesh )
    {
        // Couleur blanche
        glColor4ub ( 255 , 255 , 255 , 255 );

        if ( !m_wireframe && m_materiaux )
        {
            glDisable ( GL_COLOR_MATERIAL );
        }

        glBegin ( GL_TRIANGLES );

        const unsigned int nbr_groups = m_mesh->getNbrGroups();

        // Pour chaque groupe
        for ( unsigned int i = 0 ; i < nbr_groups ; ++i )
        {
            CMeshGroup * group = m_mesh->getGroup ( i );

            if ( !group ) continue;

            // Matériau du groupe
            if ( !m_wireframe && m_materiaux )
            {
                CMateriau * mtl = group->getMateriau();

                if ( mtl )
                {
                    float color[4];

                    CColor c1 = mtl->getAmbient();
                    c1.toFloat ( color );
                    glMaterialfv ( GL_FRONT_AND_BACK , GL_AMBIENT , color );

                    CColor c2 = mtl->getDiffuse();
                    c2.toFloat ( color );
                    glMaterialfv ( GL_FRONT_AND_BACK , GL_DIFFUSE , color );

                    CColor c3 = mtl->getSpecular();
                    c3.toFloat ( color );
                    glMaterialfv ( GL_FRONT_AND_BACK , GL_SPECULAR , color );

                    glMaterialf ( GL_FRONT_AND_BACK , GL_SHININESS , mtl->getShininess() );
                }
                else
                {
                    SetDefaultMaterial();
                }
            }

            const unsigned int nbr_faces = group->getNbrFaces();

            // Pour chaque face
            for ( unsigned int j = 0 ; j < nbr_faces ; ++j )
            {
                CFace f = group->getFace ( j );

                // Couleur de la face
                if ( !m_wireframe && !m_materiaux )
                {
                    glColor4ub ( f.color.getRed() , f.color.getGreen() , f.color.getBlue() , f.color.getAlpha() );
                }

                CSommet s1 = m_mesh->getSommet ( f.s1 );
                CSommet s2 = m_mesh->getSommet ( f.s2 );
                CSommet s3 = m_mesh->getSommet ( f.s3 );

                if ( m_smooth )
                {
                    glNormal3fv ( s1.getNormale() );
                    glVertex3fv ( s1.getPoint() );

                    glNormal3fv ( s2.getNormale() );
                    glVertex3fv ( s2.getPoint() );

                    glNormal3fv ( s3.getNormale() );
                    glVertex3fv ( s3.getPoint() );
                }
                else
                {
                    glNormal3fv ( group->NormaleFace ( j ) );

                    glVertex3fv ( s1.getPoint() );
                    glVertex3fv ( s2.getPoint() );
                    glVertex3fv ( s3.getPoint() );
                }
            }
        }

        glEnd();


        glEnable ( GL_COLOR_MATERIAL );
        if ( m_culling ) glDisable ( GL_CULL_FACE );
        if ( m_wireframe ) glPolygonMode ( GL_FRONT_AND_BACK , GL_FILL );


        // Affichage des vecteurs normaux
        if ( m_normales )
        {
            // Couleur grise
            glColor4ub ( 128 , 128 , 128 , 255 );

            if ( m_smooth )
            {
                unsigned int nbr_sommets = m_mesh->getNbrSommets();

                // Pour chaque sommet
                for ( unsigned int i = 0 ; i < nbr_sommets ; ++i )
                {
                    CSommet s = m_mesh->getSommet ( i );
                    CVector3F p = s.getPoint();
                    CVector3F n = s.getNormale();

                    glBegin ( GL_LINES );
                    glVertex3fv ( p );
                    glVertex3fv ( p + n / 10.0f );
                    glEnd();
                }
            }
            else
            {
                unsigned int nbr_groups = m_mesh->getNbrGroups();

                // Pour chaque groupe
                for ( unsigned int i = 0 ; i < nbr_groups ; ++i )
                {
                    CMeshGroup * group = m_mesh->getGroup ( i );

                    if ( !group ) continue;

                    unsigned int nbr_faces = group->getNbrFaces();

                    // Pour chaque face
                    for ( unsigned int j = 0 ; j < nbr_faces ; ++j )
                    {
                        CVector3F b = group->BarycentreFace ( j );

                        glBegin ( GL_LINES );
                        glVertex3fv ( b );
                        glVertex3fv ( b + group->NormaleFace ( j ) / 10.0f );
                        glEnd();
                    }
                }
            }
        }


        // Affichage de la bounding box
        if ( m_boundingbox )
        {
            CVector3F min = m_mesh->getMin();
            CVector3F max = m_mesh->getMax();

            glDisable ( GL_LIGHTING );

            glEnable ( GL_BLEND );
            glBlendFunc ( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA );
            glEnable ( GL_ALPHA_TEST );
            glAlphaFunc ( GL_GREATER , 0.0f );

            glColor4ub ( 255 , 0 , 0 , 100 );

            glBegin ( GL_TRIANGLES );

            glVertex3f ( min.X , min.Y , min.Z );
            glVertex3f ( min.X , max.Y , max.Z );
            glVertex3f ( min.X , min.Y , max.Z );

            glVertex3f ( min.X , min.Y , min.Z );
            glVertex3f ( min.X , max.Y , min.Z );
            glVertex3f ( min.X , max.Y , max.Z );

            glVertex3f ( max.X , min.Y , min.Z );
            glVertex3f ( max.X , max.Y , max.Z );
            glVertex3f ( max.X , min.Y , max.Z );

            glVertex3f ( max.X , min.Y , min.Z );
            glVertex3f ( max.X , max.Y , min.Z );
            glVertex3f ( max.X , max.Y , max.Z );

            glVertex3f ( min.X , min.Y , min.Z );
            glVertex3f ( min.X , max.Y , min.Z );
            glVertex3f ( max.X , max.Y , min.Z );

            glVertex3f ( min.X , min.Y , min.Z );
            glVertex3f ( max.X , max.Y , min.Z );
            glVertex3f ( max.X , min.Y , min.Z );

            glVertex3f ( min.X , min.Y , max.Z );
            glVertex3f ( min.X , max.Y , max.Z );
            glVertex3f ( max.X , max.Y , max.Z );

            glVertex3f ( min.X , min.Y , max.Z );
            glVertex3f ( max.X , max.Y , max.Z );
            glVertex3f ( max.X , min.Y , max.Z );

            glVertex3f ( min.X , min.Y , min.Z );
            glVertex3f ( min.X , min.Y , max.Z );
            glVertex3f ( max.X , min.Y , max.Z );

            glVertex3f ( min.X , min.Y , min.Z );
            glVertex3f ( max.X , min.Y , max.Z );
            glVertex3f ( max.X , min.Y , min.Z );

            glVertex3f ( min.X , max.Y , min.Z );
            glVertex3f ( min.X , max.Y , max.Z );
            glVertex3f ( max.X , max.Y , max.Z );

            glVertex3f ( min.X , max.Y , min.Z );
            glVertex3f ( max.X , max.Y , max.Z );
            glVertex3f ( max.X , max.Y , min.Z );

            glEnd();

            glDisable ( GL_BLEND );

            glEnable ( GL_LIGHTING );
        }

        
        // Affichage de la sphère englobante
        if ( m_spherebox )
        {
            CVector3F min = m_mesh->getMin();
            CVector3F max = m_mesh->getMax();

            CVector3F diag = ( max - min ) * 0.5;
            float radius = diag.Norme();
            CVector3F center = min + diag;

            //...
        }


        if ( m_wireframe ) glPolygonMode ( GL_FRONT_AND_BACK , GL_LINE );
        if ( m_culling ) glEnable ( GL_CULL_FACE );
    }


    // Affichage du repère
    if ( m_repere )
    {
        glColor4ub ( 255 , 0 , 0 , 255 );

        glBegin ( GL_LINES );
        glVertex3f ( 0.0f , 0.0f , 0.0f );
        glVertex3f ( 1.0f , 0.0f , 0.0f );
        glEnd();

        glColor4ub ( 0 , 255 , 0 , 255 );

        glBegin ( GL_LINES );
        glVertex3f ( 0.0f , 0.0f , 0.0f );
        glVertex3f ( 0.0f , 1.0f , 0.0f );
        glEnd();

        glColor4ub ( 0 , 0 , 255 , 255 );

        glBegin ( GL_LINES );
        glVertex3f ( 0.0f , 0.0f , 0.0f );
        glVertex3f ( 0.0f , 0.0f , 1.0f );
        glEnd();
    }


    return;
}


void CDraw::mousePressEvent ( QMouseEvent * event )
{
    m_last_pos = event->pos();
    return;
}


void CDraw::mouseMoveEvent ( QMouseEvent * event )
{
    if ( event->buttons() & Qt::LeftButton )
    {
        m_angle_z += static_cast<float>( event->x() - m_last_pos.x() );
        m_angle_y -= static_cast<float>( event->y() - m_last_pos.y() );

        updateGL();
    }

    m_last_pos = event->pos();
    return;
}


void CDraw::SetDefaultMaterial ( void ) const
{
    float c1[4] = { 0.4f , 0.4f , 0.4f , 1.0f };
    glMaterialfv ( GL_FRONT_AND_BACK , GL_AMBIENT , c1 );

    float c2[4] = { 0.8f , 0.8f , 0.8f , 1.0f };
    glMaterialfv ( GL_FRONT_AND_BACK , GL_DIFFUSE , c2 );

    float c3[4] = { 0.3f , 0.3f , 0.3f , 1.0f };
    glMaterialfv ( GL_FRONT_AND_BACK , GL_SPECULAR , c3 );

    glMaterialf ( GL_FRONT_AND_BACK , GL_SHININESS , 60.0f );

    return;
}
