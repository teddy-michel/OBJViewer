/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


/*******************************
 *  Inclusions
 ******************************/

#include "CDialogScale.hpp"
#include "CMesh.hpp"


/**
 * Constructeur par défaut.
 *
 * \param parent Widget parent.
 */

CDialogScale::CDialogScale ( QWidget * parent ) :
QDialog ( parent ),
m_mesh  ( NULL ),
m_ui    ( NULL )
{
    m_ui = new Ui::CDialogScale();
    m_ui->setupUi ( this );

    connect ( m_ui->spin_x , SIGNAL(valueChanged(int)) , this , SLOT(SlotSpinX(int)) );
    connect ( m_ui->spin_y , SIGNAL(valueChanged(int)) , this , SLOT(SlotSpinY(int)) );
    connect ( m_ui->spin_z , SIGNAL(valueChanged(int)) , this , SLOT(SlotSpinZ(int)) );

    connect ( m_ui->buttonBox , SIGNAL ( clicked ( QAbstractButton * ) ) , this , SLOT ( onButtonClicked ( QAbstractButton * ) ) );
}


/**
 * Donne le maillage actuellement chargé.
 *
 * \return Maillage chargé.
 */

CMesh * CDialogScale::getMesh ( void ) const
{
    return m_mesh;
}


/**
 * Modifie le maillage chargé.
 *
 * \param mesh Maillage chargé.
 */

void CDialogScale::setMesh ( CMesh * mesh )
{
    m_mesh = mesh;
    return;
}


/**
 * Applique les modifications et ferme la boite de dialogue.
 */

void CDialogScale::onButtonClicked ( QAbstractButton * button )
{
    if ( m_ui->buttonBox->buttonRole ( button ) != QDialogButtonBox::ApplyRole )
    {
        return;
    }

    if ( m_mesh )
    {
        int vx = m_ui->spin_x->value();
        int vy = m_ui->spin_y->value();
        int vz = m_ui->spin_z->value();

        if ( vx != 100 || vy != 100 || vz != 100 )
        {
            m_mesh->Scale ( static_cast<float>(vx) / 100.0f ,
                            static_cast<float>(vy) / 100.0f ,
                            static_cast<float>(vz) / 100.0f );
        }
    }

    m_ui->spin_x->setValue ( 100 );
    m_ui->spin_y->setValue ( 100 );
    m_ui->spin_z->setValue ( 100 );

    hide();
    return;
}


void CDialogScale::SlotSpinX ( int value )
{
    if ( m_ui->check->isChecked() )
    {
        if ( m_ui->spin_y->value() != value ) m_ui->spin_y->setValue ( value );
        if ( m_ui->spin_z->value() != value ) m_ui->spin_z->setValue ( value );
    }

    return;
}


void CDialogScale::SlotSpinY ( int value )
{
    if ( m_ui->check->isChecked() )
    {
        if ( m_ui->spin_x->value() != value ) m_ui->spin_x->setValue ( value );
        if ( m_ui->spin_z->value() != value ) m_ui->spin_z->setValue ( value );
    }

    return;
}


void CDialogScale::SlotSpinZ ( int value )
{
    if ( m_ui->check->isChecked() )
    {
        if ( m_ui->spin_x->value() != value ) m_ui->spin_x->setValue ( value );
        if ( m_ui->spin_y->value() != value ) m_ui->spin_y->setValue ( value );
    }

    return;
}
