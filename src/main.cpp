/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


/*******************************
 *  Inclusions
 ******************************/

#include <QApplication>
#include "CWindow.hpp"


int main ( int argc , char * argv[] )
{
    QApplication app ( argc , argv );

    // Internationnalisation
    QString locale = QLocale::system().name().section('_', 0, 0);
    
    QTranslator translator1;
    translator1.load ( QString ( "qt_" ) + locale , QLibraryInfo::location ( QLibraryInfo::TranslationsPath ) );
    app.installTranslator ( &translator1 );

    QTranslator translator2;
    translator2.load ( QString ( "lang/OBJViewer_" ) + locale );
    app.installTranslator ( &translator2 );

    CWindow win;
    win.show();

    return app.exec();
}
