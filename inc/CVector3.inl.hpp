/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/**
 * \brief Constructeur par défaut.
 * \param x : Coordonnée X.
 * \param y : Coordonnée Y.
 * \param z : Coordonnée Z.
 ******************************/

template <class T>
inline CVector3<T>::CVector3 ( const T x , const T y , const T z ) :
X (x) , Y (y) , Z (z)
{ }


/**
 * \brief  Opération unaire +
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> CVector3<T>::operator + ( void ) const
{
    return CVector3<T> ( X , Y , Z );
}


/**
 * \brief  Opération unaire -
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> CVector3<T>::operator - ( void ) const
{
    return CVector3<T> ( -X , -Y , -Z );
}


/**
 * \brief  Opération binaire +
 * \param  vecteur : Vecteur à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> CVector3<T>::operator + ( const CVector3<T>& vecteur ) const
{
    return CVector3<T> ( X + vecteur.X , Y + vecteur.Y , Z + vecteur.Z );
}


/**
 * \brief  Opération binaire -
 * \param  vecteur : Vecteur à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> CVector3<T>::operator - ( const CVector3<T>& vecteur ) const
{
    return CVector3<T> ( X - vecteur.X , Y - vecteur.Y , Z - vecteur.Z );
}


/**
 * \brief  Opération +=
 * \param  vecteur : Vecteur à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector3<T>& CVector3<T>::operator += ( const CVector3<T>& vecteur )
{
    X += vecteur.X;
    Y += vecteur.Y;
    Z += vecteur.Z;

    return *this;
}

/**
 * \brief  Opération -=
 * \param  vecteur : Vecteur à soustraire
 * \return Résultat de l'opération
 ******************************/

template <class T>
inline const CVector3<T>& CVector3<T>::operator -= ( const CVector3<T>& vecteur )
{
    X -= vecteur.X;
    Y -= vecteur.Y;
    Z -= vecteur.Z;

    return *this;
}


/**
 * \brief  Multiplication par un scalaire
 * \param  k : Scalaire
 * \return Résultat de l'opération
 ******************************/

template <class T>
inline CVector3<T> CVector3<T>::operator * ( const T k ) const
{
    return CVector3<T> ( X * k , Y * k , Z * k );
}


/**
 * \brief  Division par un scalaire
 * \param  k : Scalaire
 * \return Résultat de l'opération
 ******************************/

template <class T>
inline CVector3<T> CVector3<T>::operator / ( const T k ) const
{
    return CVector3<T> ( X / k , Y / k , Z / k );
}


/**
 * \brief  Opération *=
 * \param  k : Scalaire
 * \return Résultat de l'opération
 ******************************/

template <class T>
inline const CVector3<T>& CVector3<T>::operator *= ( const T k )
{
    X *= k;
    Y *= k;
    Z *= k;

    return *this;
}


/**
 * \brief  Opération /=
 * \param  k : Scalaire
 * \return Résultat de l'opération
 ******************************/

template <class T>
inline const CVector3<T>& CVector3<T>::operator /= ( const T k )
{
    X /= k;
    Y /= k;
    Z /= k;

    return *this;
}


/**
 * \brief  Comparaison ==.
 * \param  vecteur : Vecteur à comparer.
 * \return Booléen.
 ******************************/

template <class T>
inline bool CVector3<T>::operator == ( const CVector3<T>& vecteur ) const
{
    return ( ( std::abs ( X - vecteur.X ) <= std::numeric_limits<T>::epsilon () ) &&
             ( std::abs ( Y - vecteur.Y ) <= std::numeric_limits<T>::epsilon () ) &&
             ( std::abs ( Z - vecteur.Z ) <= std::numeric_limits<T>::epsilon () ) );
}


/**
 * \brief  Comparaison !=.
 * \param  vecteur : Vecteur à comparer.
 * \return Booléen.
 ******************************/

template <class T>
inline bool CVector3<T>::operator != ( const CVector3<T>& vecteur ) const
{
    return !( *this == vecteur );
}


/**
 * \brief Cast en T*.
 ******************************/

template <class T>
inline CVector3<T>::operator T* ( void )
{
    return &X;
}


/**
 * \brief Réinitialise le vecteur.
 * \param x : Composante x du vecteur.
 * \param y : Composante y du vecteur.
 * \param z : Composante z du vecteur.
 ******************************/

template <class T>
inline void CVector3<T>::Set ( const T x , const T y , const T z )
{
    X = x;
    Y = y;
    Z = z;

    return;
}


/**
 * \brief  Calcul la norme du vecteur.
 * \return Norme du vecteur.
 ******************************/

template <class T>
inline T CVector3<T>::Norme ( void ) const
{
    return sqrt ( X*X + Y*Y + Z*Z );
}


/**
 * \brief Normalise le vecteur.
 ******************************/

template <class T>
inline void CVector3<T>::Normalize ( void )
{
    T norme = Norme();

    if ( std::abs ( norme ) > std::numeric_limits<T>::epsilon() )
    {
        X /= norme;
        Y /= norme;
        Z /= norme;
    }

    return;
}


/**
 * \brief  Multiplication d'un vecteur par un scalaire.
 * \param  k : Scalaire.
 * \param  vecteur : Vecteur à multiplier.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> operator * ( const CVector3<T>& vecteur , const T k )
{
    return vecteur * k;
}


/**
 * \brief  Division d'un vecteur par un scalaire.
 * \param  k : Scalaire.
 * \param  vecteur : Vecteur à diviser.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> operator / ( const CVector3<T>& vecteur , const T k )
{
    return vecteur / k;
}


/**
 * \brief  Multiplication d'un vecteur par un scalaire.
 * \param  k : Scalaire.
 * \param  vecteur : Vecteur à multiplier.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> operator * ( const T k , const CVector3<T>& vecteur )
{
    return vecteur * k;
}


/**
 * \brief  Effectue le produit scalaire de deux vecteurs.
 * \param  v1 : Vecteur 1.
 * \param  v2 : Vecteur 2.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline T VectorDot ( const CVector3<T>& v1 , const CVector3<T>& v2 )
{
    return ( v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z );
}


/**
 * \brief  Effectue le produit vectoriel de deux vecteurs.
 * \param  v1 : Vecteur 1.
 * \param  v2 : Vecteur 2.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> VectorCross ( const CVector3<T>& v1 , const CVector3<T>& v2 )
{
    return CVector3<T> ( v1.Y * v2.Z - v1.Z * v2.Y , v1.Z * v2.X - v1.X * v2.Z , v1.X * v2.Y - v1.Y * v2.X );
}


/**
 * Surcharge de l'opérateur >> entre un flux et un vecteur.
 *
 * \param flux    Flux d'entrée.
 * \param vecteur Vecteur.
 * \return Référence sur le flux d'entrée.
 ******************************/

template <class T>
inline QTextStream& operator >> ( QTextStream& flux , CVector3<T>& vecteur )
{
    return flux >> vecteur.X >> vecteur.Y >> vecteur.Z;
}


/**
 * Surcharge de l'opérateur << entre un flux et un vecteur.
 *
 * \param flux    Flux de sortie.
 * \param vecteur Vecteur.
 * \return Référence sur le flux de sortie.
 ******************************/

template <class T>
inline QTextStream& operator << ( QTextStream& flux , const CVector3<T>& vecteur )
{
    return flux << QString::number ( vecteur.X ) << QString ( " " ) << QString::number ( vecteur.Y ) << QString ( " " ) << QString::number ( vecteur.Z );
}
