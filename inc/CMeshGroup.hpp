/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef FILE_HEADER_CMESHGROUP
#define FILE_HEADER_CMESHGROUP


/*******************************
 *  Inclusions
 ******************************/

#include <QString>
#include <vector>

#include "CVector3.hpp"
#include "CColor.hpp"


class CMesh;
class CMateriau;


/// Face triangulaire.
struct CFace
{
    unsigned int s1; ///< Premier sommet.
    unsigned int s2; ///< Deuxième sommet.
    unsigned int s3; ///< Troisième sommet.
    CColor color;    ///< Couleur de la face

    /**
     * Constructeur par défaut.
     *
     * \param id1 Indice du premier sommet.
     * \param id2 Indice du deuxième sommet.
     * \param id3 Indice du troisième sommet.
     * \param c Couleur de la face (blanche par défaut).
     */

    CFace ( unsigned int id1 = 0 , unsigned int id2 = 0 , unsigned int id3 = 0 , const CColor& c = CColor::Blanc ) :
    s1 ( id1 ), s2 ( id2 ), s3 ( id3 ), color ( c ) { }
};


class CMeshGroup
{
public:

    // Constructeur
    CMeshGroup ( CMesh * mesh , const QString& name = "" );

    // Accesseurs
    QString getName ( void ) const;
    CMateriau * getMateriau ( void ) const;
    CMesh * getMesh ( void ) const;
    CFace getFace ( unsigned int indice ) const;

    // Mutateurs
    void setName ( const QString& name );
    void setMateriau ( CMateriau * materiau );
    void setMesh ( CMesh * mesh );

    // Méthodes publiques
    unsigned int getNbrFaces ( void ) const;
    CVector3F NormaleFace ( unsigned int indice ) const;
    CVector3F NormaleFace ( const CFace& face ) const;
    CVector3F BarycentreFace ( unsigned int indice ) const;
    CVector3F BarycentreFace ( const CFace& face ) const;
    void AddFace ( unsigned int s1 , unsigned int s2 , unsigned int s3 );
    void SubdivisionPlane ( void );
    void SubdivisionNormale ( void );
    void RemplaceIndiceSommet ( unsigned int old_indice , unsigned int new_indice );

protected:

    // Données protégées
    QString m_name;			    ///< Nom du groupe.
    CMateriau * m_materiau;	    ///< Matériau du groupe.
    CMesh * m_mesh;             ///< Mesh auquel appartient ce groupe.
    std::vector<CFace> m_faces; ///< Tableau contenant les faces du groupe.
};

#endif
