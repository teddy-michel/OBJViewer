/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef FILE_HEADER_CMATERIAU
#define FILE_HEADER_CMATERIAU


/*******************************
 *  Inclusions
 ******************************/

#include <QString>

#include "CColor.hpp"


class CMateriau
{
public:

    // Constructeur
    CMateriau ( const QString& name );

    // Accesseurs
    QString getName ( void ) const;
    CColor getAmbient ( void ) const;
    CColor getDiffuse ( void ) const;
    CColor getSpecular ( void ) const;
    float getTransparency ( void ) const;
    float getShininess ( void ) const;
    bool getIllumination ( void ) const;

    // Mutateurs
    void setName ( const QString& name );
    void setAmbient ( const CColor& color );
    void setDiffuse ( const CColor& color );
    void setSpecular ( const CColor& color );
    void setTransparency ( float transparency );
    void setShininess ( float shininess );
    void setIllumination ( bool illum = true );

protected:

    // Données protégées
    QString m_name;       ///< Nom du matériau.
    CColor m_ambient;     ///< Couleur ambiante.
    CColor m_diffuse;     ///< Couleur diffuse.
    CColor m_specular;    ///< Couleur spéculaire.
    float m_transparency; ///< Transparence (comprise entre 0 et 1).
    float m_shininess;    ///< Brillance.
    bool m_illumination;  ///< Indique si le matériau doit être illuminé avec le couleur spéculaire.
};

#endif
