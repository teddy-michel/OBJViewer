/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef FILE_HEADER_CDRAW
#define FILE_HEADER_CDRAW


/*******************************
 *  Inclusions
 ******************************/

#include <QtGui>
#include <QGLWidget>


class CMesh;


class CDraw : public QGLWidget
{
public:

    CDraw ( QWidget * parent = 0 , QGLWidget *shareWidget = 0 );

    CMesh * getMesh ( void ) const;

    void setMesh ( CMesh * mesh );
    void ToggleRepere ( void );
    void ToggleBoundingBox ( void );
    void ToggleSphereBox ( void );
    void ToggleNormales ( void );
    void ToggleCulling ( void );
    void ToggleSmooth ( void );
    void ToggleWireframe ( void );

    QSize minimumSizeHint ( void ) const;
    QSize sizeHint ( void ) const;

private:

    void SetDefaultMaterial ( void ) const;

protected:

    void initializeGL ( void );
    void resizeGL ( int w , int h );
    void paintGL ( void );
    void mousePressEvent ( QMouseEvent * event );
    void mouseMoveEvent ( QMouseEvent * event );

    CMesh * m_mesh;     ///< Pointeur sur le maillage � afficher.
    bool m_smooth;      ///< Indique si on utilise les normales des sommets.
    bool m_normales;    ///< Indique si on affiche les normales.
    bool m_repere;      ///< Indique si on affiche le rep�re.
    bool m_wireframe;   ///< Indique si on affiche en mode fil-de-fer.
    bool m_culling;     ///< Indique si le culling est activ�.
    bool m_boundingbox; ///< Indique si on affiche la boite englobante.
    bool m_spherebox;   ///< Indique si on affiche la sph�re englobante.
    bool m_materiaux;   ///< Indique si on utilise les mat�riaux ou les couleurs des faces.
    QPoint m_last_pos;  ///< Derni�re position.
    float m_angle_z;
    float m_angle_y;
};

#endif
