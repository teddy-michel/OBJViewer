/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


/**
 * \brief Constructeur par défaut.
 * \param x : Coordonnée X.
 * \param y : Coordonnée Y.
 ******************************/

template <class T>
inline CVector2<T>::CVector2 ( const T x , const T y ) :
X (x) , Y (y)
{ }


/**
 * \brief  Opération unaire +.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> CVector2<T>::operator + ( void ) const
{
    return CVector2 ( X , Y );
}


/**
 * \brief  Opération unaire -.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> CVector2<T>::operator - ( void ) const
{
    return CVector2<T> ( -X , -Y );
}


/**
 * \brief  Opération binaire +.
 * \param  vecteur : Vecteur à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> CVector2<T>::operator + ( const CVector2<T>& vecteur ) const
{
    return CVector2<T> ( X + vecteur.X , Y + vecteur.Y );
}


/**
 * \brief  Opération binaire -.
 * \param  vecteur : Vecteur à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> CVector2<T>::operator - ( const CVector2<T>& vecteur ) const
{
    return CVector2<T> ( X - vecteur.X , Y - vecteur.Y );
}


/**
 * \brief  Opération +=.
 * \param  vecteur : Vecteur à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector2<T>& CVector2<T>::operator += ( const CVector2<T>& vecteur )
{
    X += vecteur.X;
    Y += vecteur.Y;

    return *this;
}

/**
 * \brief Opération -=.
 * \param vecteur : Vecteur à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector2<T>& CVector2<T>::operator -= ( const CVector2<T>& vecteur )
{
    X -= vecteur.X;
    Y -= vecteur.Y;

    return *this;
}


/**
 * \brief  Multiplication par un scalaire.
 * \param  k : Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> CVector2<T>::operator * ( const T k ) const
{
    return CVector2<T> ( X * k , Y * k );
}


/**
 * \brief  Division par un scalaire.
 * \param  k : Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> CVector2<T>::operator / ( const T k ) const
{
    return CVector2<T> ( X / k , Y / k );
}


/**
 * \brief  Opération *=.
 * \param  k : Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector2<T>& CVector2<T>::operator *= ( const T k )
{
    X *= k;
    Y *= k;

    return *this;
}


/**
 * \brief  Opération /=.
 * \param  k : Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector2<T>& CVector2<T>::operator /= ( const T k )
{
    X /= k;
    Y /= k;

    return *this;
}


/**
 * \brief  Opérateur de comparaison (==).
 * \param  vecteur : Vecteur à comparer.
 * \return Booléen.
 ******************************/

template <class T>
inline bool CVector2<T>::operator == ( const CVector2<T>& vecteur ) const
{
    return ( ( std::abs ( X - vecteur.X ) <= std::numeric_limits<T>::epsilon() ) &&
             ( std::abs ( Y - vecteur.Y ) <= std::numeric_limits<T>::epsilon() ) );
}


/**
 * \brief  Opérateur de comparaison (!=).
 * \param  vecteur : Vecteur à comparer.
 * \return Booléen.
 ******************************/

template <class T>
inline bool CVector2<T>::operator != ( const CVector2<T>& vecteur ) const
{
    return !( *this == vecteur );
}


/**
 * \brief Cast en T*.
 ******************************/

template <class T>
inline CVector2<T>::operator T* ( void )
{
    return &X;
}


/**
 * \brief Réinitialise le vecteur.
 * \param x : composante x du vecteur.
 * \param y : composante y du vecteur.
 ******************************/

template <class T>
inline void CVector2<T>::Set ( const T x , const T y )
{
    X = x;
    Y = y;

    return;
}


/**
 * \brief  Calcule la norme du vecteur.
 * \return Norme du vecteur.
 ******************************/

template <class T>
inline T CVector2<T>::Norme ( void ) const
{
    return sqrt ( X*X + Y*Y );
}


/**
 * \brief Normalise le vecteur.
 ******************************/

template <class T>
inline void CVector2<T>::Normalize ( void )
{
    T norme = Norme();

    if ( norme > std::numeric_limits<T>::epsilon() )
    {
        X /= norme;
        Y /= norme;
    }

    return;
}


/**
 * \brief  Opérateur de multiplication avec un scalaire.
 * \param  vecteur : vecteur.
 * \param  k : scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> operator * ( const CVector2<T>& vecteur , const T k )
{
    return CVector2<T> ( vecteur.X * k , vecteur.Y * k );
}


/**
 * \brief  Opérateurs de division par un scalaire.
 * \param  vecteur : vecteur.
 * \param  k : scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> operator / ( const CVector2<T>& vecteur , const T k )
{
    return CVector2<T> ( vecteur.X / k , vecteur.Y / k );
}


/**
 * \brief  Opérateur de multiplication avec un scalaire.
 * \param  k : scalaire.
 * \param  vecteur : vecteur.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> operator * ( const T k , const CVector2<T>& vecteur )
{
    return vecteur * k;
}


/**
 * \brief  Effectue le produit scalaire de deux vecteurs.
 * \param  v1 : Vecteur 1.
 * \param  v2 : Vecteur 2.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline T VectorDot ( const CVector2<T>& v1 , const CVector2<T>& v2 )
{
    return ( v1.X * v2.X + v1.Y * v2.Y );
}


/**
 * Surcharge de l'opérateur >> entre un flux et un vecteur.
 *
 * \param flux    Flux d'entrée.
 * \param vecteur Vecteur.
 * \return Référence sur le flux d'entrée.
 ******************************/

template <class T>
inline QTextStream& operator >> (QTextStream& flux , CVector2<T>& vecteur )
{
    return flux >> vecteur.X >> vecteur.Y;
}


/**
 * Surcharge de l'opérateur << entre un flux et un vecteur.
 *
 * \param flux    Flux de sortie.
 * \param vecteur Vecteur.
 * \return Référence sur le flux de sortie.
 ******************************/

template <class T>
inline QTextStream& operator << ( QTextStream& flux , const CVector2<T>& vecteur )
{
    return flux << QString::number ( vecteur.X ) << QString ( " " ) << QString::number ( vecteur.Y );
}
