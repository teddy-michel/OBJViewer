/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef FILE_HEADER_CCOLOR
#define FILE_HEADER_CCOLOR


/*******************************
 *  Inclusions
 ******************************/

#include <QString>


/**
 * \class CColor
 *
 * Description d'une couleur.
 * Classe permettant de gérer les couleurs. Un certain nombre de méthodes sont
 * destinées à modifier ces couleurs : conversion en niveau de gris, modulation, etc.
 * La couleur est stockée sous forme d'un nombre entier de 4 octets au format RGBA.
 ******************************/

class CColor
{
public:

    // Constructeurs
    CColor ( unsigned int color = 0xFFFFFFFF );
    CColor ( unsigned char r , unsigned char g , unsigned char b , unsigned char a = 0xFF );

    // Accesseur
    unsigned int getColor ( void ) const;

    // Opérateurs
    bool operator == ( const CColor& color ) const;
    bool operator != ( const CColor& color ) const;
    const CColor& operator += ( const CColor& color );
    const CColor& operator -= ( const CColor& color );
    CColor operator + ( const CColor& color ) const;
    CColor operator - ( const CColor& color ) const;
    CColor operator * ( float k ) const;
    const CColor& operator *= ( float k );
    CColor operator / ( float k ) const;
    const CColor& operator /= ( float k );

    // Méthodes publiques
    unsigned char toGris ( void ) const;
    unsigned int toARGB ( void ) const;
    unsigned int toABGR ( void ) const;
    unsigned int toRGBA ( void ) const;
    unsigned char getAlpha ( void ) const;
    unsigned char getRed ( void ) const;
    unsigned char getGreen ( void ) const;
    unsigned char getBlue ( void ) const;
    void set ( float r , float g , float b , float a = 1.0f );
    void set ( unsigned char r , unsigned char g , unsigned char b , unsigned char a = 0xFF );
    CColor add ( const CColor& color ) const;
    CColor modulate ( const CColor& color ) const;
    void toFloat ( float dest[] ) const;
	QString toString ( void ) const;

    // Couleur prédéfinies
    static const CColor Blanc;   ///< Couleur blanche
    static const CColor Gris;    ///< Couleur grise
    static const CColor Noir;    ///< Couleur noire
    static const CColor Rouge;   ///< Couleur rouge
    static const CColor Vert;    ///< Couleur verte
    static const CColor VertF;   ///< Couleur verte foncée
    static const CColor Bleu;    ///< Couleur bleue
    static const CColor Jaune;   ///< Couleur jaune
    static const CColor Cyan;    ///< Couleur cyan
    static const CColor Magenta; ///< Couleur magenta
    static const CColor Orange;  ///< Couleur orange

private:

    // Donnée privée
    unsigned int m_color; ///< Valeur de la couleur

    // Méthode protégée
    void setInt ( int r , int g , int b , int a = 0xFF );
};

#endif
