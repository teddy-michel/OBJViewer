/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef FILE_HEADER_CDIALOGINFOS
#define FILE_HEADER_CDIALOGINFOS


/*******************************
 *  Inclusions
 ******************************/

#include <QtGui>
#include "ui_CDialogInfos.h"


class CMesh;


class CDialogInfos : public QDialog
{
public:

    CDialogInfos ( QWidget * parent = 0 );

    CMesh * getMesh ( void ) const;
    void setMesh ( CMesh * mesh );

protected:

    CMesh * m_mesh;
    Ui::CDialogInfos * m_ui;
};

#endif
