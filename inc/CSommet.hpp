/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef FILE_HEADER_CSOMMET
#define FILE_HEADER_CSOMMET


/*******************************
 *  Inclusions
 ******************************/

#include "CVector3.hpp"


class CSommet
{
public:

    // Constructeurs
    CSommet ( void );
    CSommet ( const CVector3F& point , const CVector3F& normale );

    // Accesseurs
    CVector3F getPoint ( void ) const;
    CVector3F getNormale ( void ) const;

    // Mutateurs
    void setPoint ( const CVector3F& point );
    void setNormale ( const CVector3F& normale );

protected:

    // Données protégées
    CVector3F m_point;   ///< Coordonnées du sommet.
    CVector3F m_normale; ///< Vecteur normale au sommet.
};

#endif
