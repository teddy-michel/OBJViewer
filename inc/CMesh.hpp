/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef FILE_HEADER_CMESH
#define FILE_HEADER_CMESH


/*******************************
 *  Inclusions
 ******************************/

#include <QString>
#include <list>
#include <vector>

#include "CSommet.hpp"
#include "CMeshGroup.hpp"


class CMateriau;


class CMesh
{
public:

    CMesh ( void );
    ~CMesh();

    QString getName ( void ) const;
    QString getFilename ( void ) const;
    QString getHeader ( void ) const;
    CSommet getSommet ( unsigned int indice ) const;
    CMeshGroup * getGroup ( unsigned int indice ) const;
    CMateriau * getMateriau ( unsigned int indice ) const;
    void setHeader ( const QString& header );

    bool LoadFromFile ( const QString& filename );
    bool SaveToFile ( const QString& filename ) const;
    unsigned int getNbrGroups ( void ) const;
    unsigned int getNbrSommets ( void ) const;
    unsigned int getNbrFaces ( void ) const;
    unsigned int getNbrMateriaux ( void ) const;
    unsigned int AddSommet ( const CSommet& sommet );
    void SubdivisionPlane ( void );
    void SubdivisionNormale ( void );
    void Filtre ( const std::vector<float>& coefs );
    void Scale ( float scale );
    void Scale ( float x , float y , float z );
    void Rotate ( float x , float y , float z );
    CVector3F getMin ( void ) const;
    CVector3F getMax ( void ) const;
    CVector3F getSize ( void ) const;
    std::vector< std::vector<unsigned int> > SommetsVoisins ( unsigned int sommet , unsigned int niveau = 1 ) const;
    std::vector<CFace> FacesVoisines ( const CFace& face ) const;
    std::vector<CFace> FacesIncidentes ( unsigned int indice ) const;
    void ComputeNormaleSommet ( unsigned int indice );
    void SupprimeSommetsIdentiques ( void );

private:

    bool ReadMTL ( const QString& filename );
    bool WriteMTL ( const QString& filename ) const;

protected:

    QString m_filename;                 ///< Nom du fichier OBJ.
    QString m_header;                   ///< En-tête du fichier OBJ.
    std::vector<CSommet> m_sommets;     ///< Tableau contenant les sommets du groupe.
    std::list<CMeshGroup *> m_groups;   ///< Liste des groupes du maillage.
    std::list<CMateriau *> m_materiaux; ///< Liste des matériaux utilisés par le maillage.
};

#endif
