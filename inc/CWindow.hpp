/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef FILE_HEADER_CWINDOW
#define FILE_HEADER_CWINDOW


/*******************************
 *  Inclusions
 ******************************/

#include <QtGui>
#include <QString>


class CMesh;
class CDraw;
class CDialogInfos;
class CDialogGroups;
class CDialogMateriaux;
class CDialogScale;
class CDialogRotate;


class CWindow : public QMainWindow
{
    Q_OBJECT

public:

    // Constructeur et destructeur
    CWindow ( void );
    ~CWindow();

public slots:

    // Slots
    void OpenFile ( void );
    void SaveFile ( void );
    void SaveFileAs ( void );
    void Close ( void );
    void DialogInfos ( void );
    void DialogGroups ( void );
    void DialogMateriaux ( void );
    void DialogScale ( void );
    void DialogRotate ( void );
    void SubdivisionPlane ( void );
    void SubdivisionNormale ( void );
    void Filtrage ( void );
    void AffRepere ( void );
    void AffBoundingBox ( void );
    void AffSphereBox ( void );
    void AffNormales ( void );
    void AffCulling ( void );
    void AffSmooth ( void );
    void AffWireframe ( void );

private:

    void SaveFile ( const QString& filename ) const;

protected:

    // Donnée protégée
    CMesh * m_mesh;  ///< Modèle actuellement chargé.
    CDraw * m_draw;  ///< Widget OpenGL.

    CDialogInfos * m_dialog_infos;
    CDialogGroups * m_dialog_groups;
    CDialogMateriaux * m_dialog_materiaux;
    CDialogScale * m_dialog_scale;
    CDialogRotate * m_dialog_rotate;

    QMenu * m_menu_fichier;
    QMenu * m_menu_modele;
    QMenu * m_menu_affichage;
    QMenu * m_menu_aide;

    QAction * m_action_save;
    QAction * m_action_saveas;
    QAction * m_action_close;
    QAction * m_action_infos;
    QAction * m_action_groupes;
    QAction * m_action_materiaux;
    QAction * m_action_scale;
    QAction * m_action_rotate;
    QAction * m_action_subplane;
    QAction * m_action_subnormale;
    QAction * m_action_filtrage;
};

#endif
