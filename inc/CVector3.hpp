/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef FILE_HEADER_CVECTOR3
#define FILE_HEADER_CVECTOR3


/*******************************
 *  Inclusions
 ******************************/

#include <cmath>
#include <limits>
#include <QTextStream>


/**
 * \class   CVector3
 * \ingroup Maths
 * Classe pour gérer des vecteurs à 3 dimensions.
 ******************************/

template <class T>
class CVector3
{
public:

    // Données publiques
    T X; ///< Coordonnée X.
    T Y; ///< Coordonnée Y.
    T Z; ///< Coordonnée Z.

    // Constructeur
    CVector3 ( const T x = 0 , const T y = 0 , const T z = 0 );

    // Opérateurs
    CVector3<T> operator + ( void ) const;
    CVector3<T> operator - ( void ) const;
    CVector3<T> operator + ( const CVector3<T>& vecteur ) const;
    CVector3<T> operator - ( const CVector3<T>& vecteur ) const;
    const CVector3<T>& operator += ( const CVector3<T>& vecteur );
    const CVector3<T>& operator -= ( const CVector3<T>& vecteur );
    CVector3<T> operator * ( const T k ) const;
    CVector3<T> operator / ( const T k ) const;
    const CVector3<T>& operator *= ( const T k );
    const CVector3<T>& operator /= ( const T k );
    bool operator == ( const CVector3<T>& vecteur ) const;
    bool operator != ( const CVector3<T>& vecteur ) const;
    operator T* ( void );

    // Méthodes publiques
    void Set ( const T x , const T y , const T z );
    T Norme ( void ) const;
    void Normalize ( void );

    // Valeurs prédéfinies
    static const CVector3<short>  Origin3S; ///< Origine avec short
    static const CVector3<int>    Origin3I; ///< Origine avec int
    static const CVector3<float>  Origin3F; ///< Origine avec float
    static const CVector3<double> Origin3D; ///< Origine avec double
};


/*******************************
 *  Fonctions communes aux vecteurs
 ******************************/

template <class T> CVector3<T> operator * ( const CVector3<T>& vecteur , const T k );
template <class T> CVector3<T> operator / ( const CVector3<T>& vecteur , const T k );
template <class T> CVector3<T> operator * ( const T k , const CVector3<T>& vecteur );
template <class T> T VectorDot ( const CVector3<T>& v1 , const CVector3<T>& v2 );
template <class T> CVector3<T> VectorCross ( const CVector3<T>& v1 , const CVector3<T>& v2 );
template <class T> QTextStream& operator >> ( QTextStream& flux , CVector3<T>& vecteur );
template <class T> QTextStream& operator << ( QTextStream& flux , const CVector3<T>& vecteur );


/*******************************
 *  Formats usuels
 ******************************/

typedef CVector3<short>  CVector3S; ///< Vecteur de short.
typedef CVector3<int>    CVector3I; ///< Vecteur d'entiers.
typedef CVector3<float>  CVector3F; ///< Vecteur de flottants.
typedef CVector3<double> CVector3D; ///< Vecteur de doubles.


/*******************************
 *  Valeurs prédéfinies
 ******************************/

const CVector3<short>  Origin3S ( 0 , 0 , 0 );
const CVector3<int>    Origin3I ( 0 , 0 , 0 );
const CVector3<float>  Origin3F ( 0.0f , 0.0f , 0.0f );
const CVector3<double> Origin3D ( 0.0f , 0.0f , 0.0f );


#include "CVector3.inl.hpp"

#endif //FILE_HEADER
