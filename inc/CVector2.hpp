/*
Copyright (C) 2010-2012 Teddy Michel

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef FILE_HEADER_CVECTOR2
#define FILE_HEADER_CVECTOR2


/*******************************
 *  Inclusions
 ******************************/

#include <cmath>
#include <limits>
#include <QTextStream>


/**
 * \class CVector2
 * \ingroup Maths
 *
 *  Classe pour gérer des vecteurs à 2 dimensions.
 ******************************/
template <class T>
class CVector2
{
public:

    // Données publiques
    T X; ///< Coordonnée X
    T Y; ///< Coordonnée Y

    // Constructeur
    CVector2 ( const T x = 0 , const T y = 0 );

    // Opérateurs
    CVector2<T> operator + ( void ) const;
    CVector2<T> operator - ( void ) const;
    CVector2<T> operator + ( const CVector2<T>& vecteur ) const;
    CVector2<T> operator - ( const CVector2<T>& vecteur ) const;
    const CVector2<T>& operator += ( const CVector2<T>& vecteur );
    const CVector2<T>& operator -= ( const CVector2<T>& vecteur );
    CVector2<T> operator * ( const T k ) const;
    CVector2<T> operator / ( const T k ) const;
    const CVector2<T>& operator *= ( const T k );
    const CVector2<T>& operator /= ( const T k );
    bool operator == ( const CVector2<T>& vecteur ) const;
    bool operator != ( const CVector2<T>& vecteur ) const;
    operator T* ( void );

    // Méthodes publiques
    void Set ( T x , T y );
    T Norme ( void ) const;
    void Normalize ( void );

    // Valeurs prédéfinies
    static const CVector2<short>  Origin2S; ///< Origine avec short
    static const CVector2<int>    Origin2I; ///< Origine avec int
    static const CVector2<float>  Origin2F; ///< Origine avec float
    static const CVector2<double> Origin2D; ///< Origine avec double
};


/*******************************
 *  Fonctions communes aux vecteurs
 ******************************/

template <class T> CVector2<T>   operator * ( const CVector2<T>& vecteur , const T k );
template <class T> CVector2<T>   operator / ( const CVector2<T>& vecteur , const T k );
template <class T> CVector2<T>   operator * ( const T k , const CVector2<T>& vecteur );
template <class T> T             VectorDot  ( const CVector2<T>& v1 , const CVector2<T>& v2 );
template <class T> QTextStream& operator >> ( QTextStream& flux , CVector2<T>& vecteur );
template <class T> QTextStream& operator << ( QTextStream& flux , const CVector2<T>& vecteur );


/*******************************
 *  Formats usuels
 ******************************/

typedef CVector2<short>  CVector2S; ///< Vecteur de short.
typedef CVector2<int>    CVector2I; ///< Vecteur d'entiers.
typedef CVector2<float>  CVector2F; ///< Vecteur de flottants.
typedef CVector2<double> CVector2D; ///< Vecteur de doubles.


/*******************************
 *  Valeurs prédéfinies
 ******************************/

const CVector2<short>  Origin2S ( 0 , 0 );
const CVector2<int>    Origin2I ( 0 , 0 );
const CVector2<float>  Origin2F ( 0.0f , 0.0f );
const CVector2<double> Origin2D ( 0.0f , 0.0f );


#include "CVector2.inl.hpp"

#endif
