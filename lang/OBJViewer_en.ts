<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>CDialogGroups</name>
    <message>
        <location filename="../ui/CDialogGroups.ui" line="12"/>
        <source>Groupes</source>
        <translation>Groups</translation>
    </message>
    <message>
        <source>Fermer</source>
        <translation type="obsolete">Close</translation>
    </message>
    <message>
        <location filename="../src/CDialogGroups.cpp" line="55"/>
        <source>Nom</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/CDialogGroups.cpp" line="55"/>
        <source>Faces</source>
        <translation>Faces</translation>
    </message>
    <message utf8="true">
        <location filename="../src/CDialogGroups.cpp" line="55"/>
        <source>Matériau</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CDialogInfos</name>
    <message>
        <location filename="../ui/CDialogInfos.ui" line="12"/>
        <source>Informations sur le maillage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/CDialogInfos.ui" line="25"/>
        <source>Fichier :</source>
        <translation>File:</translation>
    </message>
    <message>
        <location filename="../ui/CDialogInfos.ui" line="46"/>
        <source>Nombre de groupes :</source>
        <translation>Number of groups:</translation>
    </message>
    <message>
        <location filename="../ui/CDialogInfos.ui" line="39"/>
        <source>Nombre de faces :</source>
        <translation>Number of faces:</translation>
    </message>
    <message>
        <location filename="../ui/CDialogInfos.ui" line="53"/>
        <source>Nombre de sommets :</source>
        <translation>Number of vertices:</translation>
    </message>
    <message>
        <location filename="../ui/CDialogInfos.ui" line="32"/>
        <source>Dimensions :</source>
        <translation>Size:</translation>
    </message>
    <message>
        <source>Fermer</source>
        <translation type="obsolete">Close</translation>
    </message>
</context>
<context>
    <name>CDialogMateriaux</name>
    <message utf8="true">
        <location filename="../ui/CDialogMateriaux.ui" line="12"/>
        <source>Matériaux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fermer</source>
        <translation type="obsolete">Close</translation>
    </message>
    <message>
        <location filename="../src/CDialogMateriaux.cpp" line="64"/>
        <source>Nom</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/CDialogMateriaux.cpp" line="65"/>
        <source>Couleur ambiante</source>
        <translation>Ambient color</translation>
    </message>
    <message>
        <location filename="../src/CDialogMateriaux.cpp" line="66"/>
        <source>Couleur diffuse</source>
        <translation>Diffuse color</translation>
    </message>
    <message utf8="true">
        <location filename="../src/CDialogMateriaux.cpp" line="67"/>
        <source>Couleur spéculaire</source>
        <translation>Specular color</translation>
    </message>
    <message>
        <location filename="../src/CDialogMateriaux.cpp" line="68"/>
        <source>Transparence</source>
        <translation>Transparency</translation>
    </message>
    <message>
        <location filename="../src/CDialogMateriaux.cpp" line="69"/>
        <source>Brillance</source>
        <translation>Shininess</translation>
    </message>
    <message>
        <location filename="../src/CDialogMateriaux.cpp" line="70"/>
        <source>Illumination</source>
        <translation>Illumination</translation>
    </message>
</context>
<context>
    <name>CDialogRotate</name>
    <message>
        <location filename="../ui/CDialogRotate.ui" line="12"/>
        <source>Rotation</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <location filename="../ui/CDialogRotate.ui" line="32"/>
        <source>X :</source>
        <translation>X:</translation>
    </message>
    <message>
        <location filename="../ui/CDialogRotate.ui" line="39"/>
        <source>Y :</source>
        <translation>Y:</translation>
    </message>
    <message>
        <location filename="../ui/CDialogRotate.ui" line="18"/>
        <source>Z :</source>
        <translation>Z:</translation>
    </message>
    <message utf8="true">
        <location filename="../ui/CDialogRotate.ui" line="52"/>
        <location filename="../ui/CDialogRotate.ui" line="68"/>
        <location filename="../ui/CDialogRotate.ui" line="84"/>
        <source> °</source>
        <translation> °</translation>
    </message>
    <message>
        <source>Appliquer</source>
        <translation type="obsolete">Apply</translation>
    </message>
    <message>
        <source>Annuler</source>
        <translation type="obsolete">Cancel</translation>
    </message>
</context>
<context>
    <name>CDialogScale</name>
    <message>
        <location filename="../ui/CDialogScale.ui" line="12"/>
        <source>Redimensionnement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/CDialogScale.ui" line="32"/>
        <source>X :</source>
        <translation>X:</translation>
    </message>
    <message>
        <location filename="../ui/CDialogScale.ui" line="39"/>
        <source>Y :</source>
        <translation>Y:</translation>
    </message>
    <message>
        <location filename="../ui/CDialogScale.ui" line="18"/>
        <source>Z :</source>
        <translation>Z:</translation>
    </message>
    <message>
        <location filename="../ui/CDialogScale.ui" line="49"/>
        <location filename="../ui/CDialogScale.ui" line="65"/>
        <location filename="../ui/CDialogScale.ui" line="81"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message utf8="true">
        <location filename="../ui/CDialogScale.ui" line="94"/>
        <source>Même facteur pour chaque axe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Appliquer</source>
        <translation type="obsolete">Apply</translation>
    </message>
    <message>
        <source>Annuler</source>
        <translation type="obsolete">Cancel</translation>
    </message>
</context>
<context>
    <name>CWindow</name>
    <message>
        <location filename="../src/CWindow.cpp" line="75"/>
        <source>Ouvrir</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="78"/>
        <source>Ouvre un fichier contenant un maillage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="80"/>
        <source>Enregistrer</source>
        <translation>Save</translation>
    </message>
    <message utf8="true">
        <location filename="../src/CWindow.cpp" line="83"/>
        <source>Enregistre le maillage affiché</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="86"/>
        <source>Enregistrer sous...</source>
        <translation>Save as...</translation>
    </message>
    <message utf8="true">
        <location filename="../src/CWindow.cpp" line="89"/>
        <source>Enregistre le maillage affiché dans un nouveau fichier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="92"/>
        <source>Fermer</source>
        <translation>Close</translation>
    </message>
    <message utf8="true">
        <location filename="../src/CWindow.cpp" line="95"/>
        <source>Ferme le maillage affiché</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="98"/>
        <source>Quitter</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="101"/>
        <source>Quitte le programme</source>
        <translation>Exit the application</translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="103"/>
        <source>Informations sur le maillage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="107"/>
        <source>Groupes</source>
        <translation>Groups</translation>
    </message>
    <message utf8="true">
        <location filename="../src/CWindow.cpp" line="111"/>
        <source>Matériaux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="115"/>
        <source>Redimensionner le maillage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="119"/>
        <source>Tourner le maillage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="123"/>
        <source>Subdivision plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="127"/>
        <source>Subdivision normale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="131"/>
        <source>Filtrage gaussien</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/CWindow.cpp" line="135"/>
        <source>Afficher le repère</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="139"/>
        <source>Afficher la boite englobante</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/CWindow.cpp" line="143"/>
        <source>Afficher la sphère englobante</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="147"/>
        <source>Afficher les normales</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="151"/>
        <source>Activer le culling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="156"/>
        <source>Utiliser les normales des faces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="160"/>
        <source>Mode fil-de-fer</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/CWindow.cpp" line="164"/>
        <source>À propos de Qt</source>
        <translation>About Qt</translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="168"/>
        <source>Fichier</source>
        <translation>File</translation>
    </message>
    <message utf8="true">
        <location filename="../src/CWindow.cpp" line="177"/>
        <source>Modèle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="190"/>
        <source>Affichage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="201"/>
        <source>Aide</source>
        <translation>Help</translation>
    </message>
    <message utf8="true">
        <location filename="../src/CWindow.cpp" line="208"/>
        <source>Aucun maillage chargé</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="230"/>
        <source>Ouvrir un fichier</source>
        <translation>Open a file</translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="230"/>
        <location filename="../src/CWindow.cpp" line="301"/>
        <source>Fichier OBJ (*.obj)</source>
        <translation>OBJ file (*.obj)</translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="238"/>
        <source>Chargement du fichier %1...</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/CWindow.cpp" line="259"/>
        <source>Chargement du fichier %1 terminé (%2 secondes)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="285"/>
        <source>Erreur lors du chargement du fichier %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/CWindow.cpp" line="521"/>
        <source>Modèle enregistré dans %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/CWindow.cpp" line="525"/>
        <source>Erreur lors de l&apos;enregistrement du modèle dans %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CWindow.cpp" line="301"/>
        <source>Enregistrer un fichier</source>
        <translation>Save a file</translation>
    </message>
</context>
</TS>
